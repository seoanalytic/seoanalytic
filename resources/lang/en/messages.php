<?php

return [
    'language' => 'Tiếng Việt',
    'phone' => 'Phone Number',
    'address' => 'Address',
    'login' => 'Login',
    'login_facebook' => 'Login with Facebook',
    'login_google' => 'Login with Google',
    'logout' => 'Logout',
    'register' => 'Register',
    'forgot_password' => 'Forgot Password?',
    'empty_cart' => 'Không có sản phẩm được chọn.',
    'empty_cart_link' => 'Tiếp tục mua hàng',
    /** Delete */
    'title_delete' => 'Bạn có thực sự muốn xóa :name_object này?',
    'text_delete' => 'Bạn không thể khôi phục :name_object sau khi xóa.',
    'title_delete_success' => 'Đã xóa!',
    'text_delete_success' => 'Xóa :name_object thành công!',
    'title_delete_fail' => 'Không thể xóa!',
    'text_delete_fail' => 'Có lỗi! Xóa :name_object thất bại!',
    /** Update status */
    'title_status' => 'Cập nhật trạng thái :name_object',
    'text_status_news' => 'Bạn có muốn :name_action bản tin này không?',
    'status_news_hide' => 'ẩn',
    'status_news_show' => 'hiển thị',
    'status_news_publish' => 'đăng',
    'text_status_product' => 'Sản phẩm này :name_action?',
    'status_product_outstock' => 'hết hàng',
    'status_product_instock' => 'đã có hàng',
    'text_status_admin' => 'Bạn có muốn :name_action quản trị này không?',
    'status_admin_block' => 'ngưng hoạt động',
    'status_admin_unblock' => 'kích hoạt',
    'title_update_success' => 'Đã cập nhật!',
    'text_update_success' => 'Cập nhật :name_object thành công!',
    'title_update_fail' => 'Không thể cập nhật!',
    'text_update_fail' => 'Có lỗi! Cập nhật :name_object thất bại!',
    /** Name object */
    'status_news_publish' => 'đăng',
    'district' => 'thành phố',
    'branch' => 'chi nhánh',
    'event' => 'sự kiện',
    'faq' => 'câu hỏi',
    'user' => 'người dùng',
    'news' => 'tin tức',
    'partner' => 'đối tác',
    'product' => 'sản phẩm',
    'rating' => 'đánh giá',
    'admin' => 'quản trị',
    'category' => 'danh mục',
    'payment' => 'ngân hàng',
    'slide' => 'slide',
    'note_cancel_order' => 'Ghi chú: đơn hàng chỉ được hủy khi ở trạng thái đang chờ.',
    /** how to buy */
    'how_to_buy' => 'Quy Trình Đặt Hàng <span>&#40;3 cách&#41;</span>',
    'way_contract' => 'Đặt hàng hoặc ký kết hợp đồng ',
    'way_online' => 'Đặt hàng trực tiếp tại website hoặc',
    'way_store' => 'Đặt hàng trực tiếp tại cửa hàng :',
    'way_branch' => 'Hoặc tại các chi nhánh:',
    'how_to_pay' => 'Quy Trình Thanh Toán',
    'pay_online' => '<p>Khách hàng đặt mua và điền đầy đủ các thông tin yêu cầu của đơn đặt hàng tại website</p>
                    <h4>Đối với khách hàng trong nội thành TP. HCM</h4>
                    <p>Giá ship nội thành tùy theo địa điểm giao hàng. Free ship cho đơn hàng từ 5kg trở lên. Vui lòng liên hệ để biết thêm chi tiết.</p>',
    'pay_cash' => 'Khách hàng có thể thanh toán tiền mặt sau khi kiểm tra và nhận đủ hàng tại địa chỉ giao nhận',
    'pay_bank' => 'Khách hàng có thể chuyển khoản trước cho chúng tôi qua ngân hàng sau:',
    'customer_suburban' => '<h4>Đối với khách hàng ngoại thành TP. HCM</h4>
                    <ul class="list">
                        <li>
                            Khách hàng phải chuyển khoản trước cho chúng tôi qua tài khoản như trên khi tiến hành đặt hàng.
                        </li>
                        <li>
                            Chúng tôi hỗ trợ giao hàng ra bến xe, chành xe hoặc địa điểm khách hàng yêu cầu.
                        </li>
                        <li>
                            Giá ship tùy theo đơn hàng hoặc địa điểm giao hàng. Vui lòng liên hệ để biết thêm chi tiết.
                        </li>
                    </ul>',
    /** user */
    'notify_update_success' => 'Cập nhật thành công!',
    'notify_update_fail' => 'Cập nhật thất bại.',
    'empty_category' => 'Danh sách rỗng. Vui lòng thêm danh mục!',
    'empty_hot_products' => 'Danh sách sản phẩm rỗng',
    'empty_hot_news' => 'Danh sách tin tức rỗng',
];