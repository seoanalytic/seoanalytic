<?php
return [
    'su_admin' => [
        'value' => 'Dev Quản trị hệ thống',
        'css' => 'label-primary'
    ],
    'admin' => [
        'value' => 'Quản trị hệ thống',
        'css' => 'label-warning'
    ],
    'mod' => [
        'value' => 'Dev Quản trị hệ thống',
        'css' => 'label-danger'
    ],
    'partner' => [
        'value' => 'Dev Quản trị hệ thống',
        'css' => 'label-default'
    ],
];