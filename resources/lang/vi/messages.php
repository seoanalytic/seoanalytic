<?php
return [
    /** message ask */
    'delete_header' => 'Bạn có chắc?',
    'delete_title' => 'Bạn có thực sự muốn xóa this :name_object này?',

    /** message return */
    'create_success' => 'Tạo :name_object thành công!',
    'create_fail' => 'Tạo :name_object bị lỗi!',
    'order_success' => ':Name_object thành công!',
    'order_fail' => ':Name_object thất bại!',
    'update_success' => 'Cập nhật :name_object thành công!',
    'update_fail' => 'Cập nhật :name_object bị lỗi!',
    'delete_success' => 'Xóa :name_object thành công!',
    'delete_fail' => 'Xóa :name_object bị lỗi!',
    'send_mail_success' => 'Chào bạn! Mail đã được gửi cho admin. Chúng tôi sẽ liên hệ với bạn ngay!',
    'send_mail_fail' => 'Liên hệ admin, gọi hotline để được tư vấn!',

    /**  */
    'get_fail' => ':Name_object không tìm thấy hoặc không tồn tại!',
    'error' => 'Đã xảy ra lỗi!!!',
    'not_permission' => 'Bạn không có quyền.',
    'not_role' => 'Không phận sự miễn vào.',

    /**  */
    'setting' => 'Cài đặt',
    'user' => 'Người dùng',
    'role' => 'Vai trò',
    'faq' => 'Câu hỏi',

];