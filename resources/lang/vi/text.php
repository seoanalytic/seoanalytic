<?php
return [
    /** Permissions */
    /** Auth */
    'login'                         => 'Đăng nhập',
    'register'                      => 'Đăng kí',
    'resetpass'                     => 'Khôi phục mật khẩu',
    /** User */
    'view_user'                     => 'Xem danh sách',
    'create_user'                   => 'Tạo',
    'edit_user'                     => 'Sửa',
    'delete_user'                   => 'Xóa',
    /** Role */
    'view_role'                     => 'Xem danh sách',
    'create_role'                   => 'Tạo',
    'edit_role'                     => 'Sửa',
    'delete_role'                   => 'Xóa',
    /** Setting */
    'view_setting'                  => 'Xem danh sách',
    'edit_setting'                  => 'Sửa',
    /** Faq */
    'view_faq'                     => 'Xem danh sách',
    'edit_faq'                     => 'Sửa',
    'delete_faq'                   => 'Xóa',
    'email_faq'                    => 'Gửi mail',

    'dashboard'                     => 'Dashboard',

    /** Faq */
    'hdr.list_faq'                  => 'Danh sách câu hỏi',
    'hdr.faq'                       => 'Quản lý câu hỏi',
    'hdr.edit_faq'                  => 'Phản hồi câu hỏi',

    /** Settings */
    'hdr.list_setting'              => 'Danh sách cấu hình',
    'hdr.edit_setting'              => 'Chỉnh sửa cấu hình',
    'hdr.setting'                   => 'Quản lý cấu hình',

    /** Permissions */
    'hdr.list_permissions'          => 'Danh sách quyền hạn',
    'hdr.create_permissions'        => 'Thêm quyền hạn',
    'hdr.edit_permissions'          => 'Chỉnh sửa quyền hạn',
    'hdr.permissions'               => 'Quản lý quyền hạn',

    /** User */
    'hdr.list_user'                 => 'Danh sách người dùng',
    'hdr.create_user'               => 'Thêm người dùng',
    'hdr.edit_user'                 => 'Chỉnh sửa người dùng',
    'hdr.user'                      => 'Quản lý người dùng',

    /** Role */
    'hdr.list_role'                 => 'Danh sách vai trò',
    'hdr.create_role'               => 'Thêm vai trò',
    'hdr.role'                      => 'Quản lý vai trò',

    /** Generals */
    'lbl.name'                      => 'Tên',
    'lbl.value'                     => 'Giá trị',
    'lbl.description'               => 'Mô tả',
    'lbl.email'                     => 'Email',
    'lbl.role'                      => 'Vai trò',
    'lbl.permission'                => 'Quyền hạn',
    'lbl.created_at'                => 'Ngày tạo',
    'lbl.lastest_active_at'         => 'Truy cập lần cuối',
    'lbl.status'                    => 'Trạng thái',
    'lbl.action'                    => 'Hành động',
    'lbl.password'                  => 'Mật khẩu',
    'lbl.password_confirmation'     => 'Xác nhận mật khẩu',
    'lbl.avatar'                    => 'Ảnh đại diện',
    'lbl.birthday'                  => 'Ngày sinh',
    'lbl.sex'                       => 'Giới tính',
    'lbl.is_admin'                  => 'Loại người dùng',

    'lbl.thumbnail_slider'          => 'Hình ảnh slider',
    'lbl.link_slider'               => 'Đường dẫn',
    'lbl.sort_slider'               => 'Thứ tự xuất hiện',

    'lbl.name_news'                 => 'Tên danh mục',
    'lbl.position_news'             => 'Vị trí',
    'lbl.featured_news'             => 'Danh mục hot',
    'lbl.parent_news'               => 'Danh mục cha',
    'lbl.description_news'          => 'Mô tả',
    'lbl.meta_news'                 => 'Thẻ Meta',
    'lbl.url_news'                  => 'Đường dẫn',
    'lbl.total_articles'            => 'Xuất bản/Tổng bài viết',
    'lbl.seo_name_news'             => 'Tên danh mục SEO',
    'lbl.seo_content_news'          => 'Nội dung danh mục SEO',
    'lbl.thumbnail_category'        => 'Hình ảnh danh mục',
    'lbl.event_name'                => 'Tên sự kiện (ngăn cách nhau bằng dấu \';\')',
    'lbl.event_link'                => 'Đường dẫn sự kiện (ngăn cách nhau bằng dấu \';\')',

    'lbl.title_news'                => 'Tiêu đề',
    'lbl.slug_news'                 => 'Đường dẫn',
    'lbl.intro_news'                => 'Giới thiệu',
    'lbl.date_news'                 => 'Ngày xuất bản',
    'lbl.content_news'              => 'Nội dung',
    'lbl.thumbnail_news'            => 'Hình ảnh',
    'lbl.tag_news'                  => 'Thẻ tags',
    'lbl.featured'                  => 'Bài viết hot',
    'lbl.publish'                   => 'Xuất bản',
    'lbl.view_news'                 => 'Lượt xem',
    'lbl.publisher'                 => 'Người duyệt',

    'lbl.name_singer'               => 'Tên ca sĩ',
    'lbl.intro_singer'              => 'Giới thiệu',
    'lbl.slug_singer'               => 'Đường dẫn',
    'lbl.thumbnail_singer'          => 'Hình đại diện',

    'lbl.name_type'                 => 'Thể loại nhạc',
    'lbl.description_type'          => 'Mô tả',
    'lbl.slug_type'                 => 'Đường dẫn',
    'lbl.thumbnail_type'            => 'Hình đại diện',

    'lbl.title_song'                => 'Tên bài hát',
    'lbl.description_song'          => 'Mô tả',
    'lbl.link_song'                 => 'Đường dẫn youtube',
    'lbl.slug_song'                 => 'Đường dẫn',
    'lbl.author_song'               => 'Nhạc sĩ',
    'lbl.thumbnail_song'            => 'Hình đại diện',
    'lbl.hot_song'                  => 'Bài hát Hot',
    'lbl.hot_singer'                => 'Yêu thích',
    'lbl.publish'                   => 'Xuất bản',
    'lbl.views_song'                => 'Lượt xem',

    'lbl.advertisement'             => 'Quảng cáo',
    'lbl.guide'                     => 'Hướng dẫn',

    'lbl.customer'                  => 'Khách hàng',
    'lbl.type_adv'                  => 'Loại',
    'lbl.link_pc'                   => 'Đường dẫn PC',
    'lbl.link_tb'                   => 'Đường dẫn tablet',
    'lbl.link_mb'                   => 'Đường dẫn mobile',
    'lbl.link_href'                 => 'Đường dẫn',
    'lbl.img_pc'                    => 'Hình ảnh PC',
    'lbl.img_tb'                    => 'Hình ảnh tablet',
    'lbl.img_mb'                    => 'Hình ảnh mobile',
    'lbl.start_date'                => 'Ngày bắt đầu',
    'lbl.end_date'                  => 'Ngày kết thúc',
    'lbl.position_id'               => 'Vị trí',

    'lbl.date_faq'                  => 'Ngày gửi',
    'lbl.feedback'                  => 'Phản hồi',
    'lbl.user_id'                   => 'Người dùng',
    'lbl.content'                   => 'Nội dung',

    'lbl.name_menu_item'            => 'Tên',
    'lbl.type_menu_item'            => 'Loại',
    'lbl.parent_menu_item'          => 'Menu cha',
    'lbl.url_menu_item'             => 'Đường dẫn',

    'lbl.adv_id'                    => 'Vị trí hiển thị',

    'lbl.display'                   => 'Hiển thị',
    'lbl.nodisplay'                 => 'Không hiển thị',

    'lbl.oldpass'                   => 'Mật khẩu cũ',
    'lbl.newpass'                   => 'Mật khẩu mới',
    'lbl.newpass_confirmation'      => 'Xác nhận mật khẩu mới',

    'lbl.admin'                     => 'Quản trị',
    'lbl.member'                    => 'Thường dân',
    'lbl.phone'                     => 'Số điện thoại',

    'lbl.level'                     => 'Level',
    'lbl.target'                    => 'Số lượt views',

    'lbl.type'                      => 'Loại',
    'lbl.title'                     => 'Bài',

    'lbl.title_user'                => 'Tên người thu',
    'lbl.song_views'                => 'Lượt nghe',

    'plh.name'                      => 'Nhập tên',
    'plh.email'                     => 'Nhập email',
    'plh.role'                      => 'Nhập vai trò',
    'plh.phone'                     => 'Nhập số điện thoại',
    'plh.permission'                => '',
    'plh.created_at'                => '',
    'plh.lastest_active_at'         => '',
    'plh.status'                    => '',
    'plh.action'                    => '',
    'plh.password'                  => 'Nhập mật khẩu',
    'plh.password_confirmation'     => 'Nhập xác nhận mật khẩu',
    'plh.oldpass'                   => 'Nhập mật khẩu cũ',
    'plh.newpass'                   => 'Nhập mật khẩu mới',
    'plh.newpass_confirmation'      => 'Nhập xác nhận mật khẩu mới',
    'plh.avatar'                    => 'Choose avatar',

    'plh.name_news'                 => 'Nhập tên',
    'plh.meta_news'                 => 'Nhập ký tự',
    'plh.slug_news'                 => 'Lấy mặc định nếu trống',
    'plh.seo_name_news'             => 'Nhập tên SEO',

    'plh.title_news'                => 'Nhập tiêu đề',
    'plh.slug_news'                 => 'Lấy mặc định nếu trống',
    'plh.related_news_id'           => 'Gõ ít nhất 3 ký tự',
    'plh.event_name'                => 'Ví dụ: Tên1; Tên2',
    'plh.event_link'                => 'Ví dụ: http://google.com; http://facebook.com',

    'plh.name_singer'               => 'Nhập tên ca sĩ',
    'plh.intro_singer'              => 'Nhập giới thiệu ca sĩ',

    'plh.name_type'                 => 'Nhập tên thể loại',
    'plh.description_type'          => 'Nhập mô tả thể loại',

    'plh.title_song'                => 'Nhập tên bài hát',
    'plh.description_song'          => 'Nhập mô tả lời bài hát',
    'plh.author_song'               => 'Nhập tên nhạc sĩ',
    'plh.link_song'                 => 'Nhập link youtube của bài hát',
    'plh.views'                      => 'Lấy mặc định là 0 nếu để trống',

    'plh.customer'                  => 'Nhập tên khách hàng',
    'plh.link_href'                 => 'Đường dẫn',

    'plh.link_slider'               => 'Nhập đường dẫn',
    'plh.sort_slider'               => 'Lấy mặc định là 0',

    'plh.name_menu_item'            => 'Nhập tên menu',
    'plh.url_menu_item'             => 'Nhập đường dẫn menu',
    'plh.link_menu_item'            => 'Nhập đường dẫn menu',

    'plh.content'                   => 'Nhập nội dung',
    'plh.feedback'                  => 'Nhập nội dung',

    'plh.level'                     => 'Nhập tên level',
    'plh.target'                    => 'Nhập số lượt views của level',

    'plh.role_name'                 => 'Nhập tên vai trò',

    'btn.save'                      => 'Lưu',
    'btn.ok'                        => 'Đồng ý',
    'btn.cancel'                    => 'Hủy',
    'btn.add'                       => 'Thêm',
    'btn.create'                    => 'Thêm',
    'btn.edit'                      => 'Sửa',
    'btn.update'                    => 'Cập nhật',
    'btn.delete'                    => 'Xóa',
    'btn.draft'                     => 'Lưu nháp',
    'btn.reply'                     => 'Phản hồi',
    'btn.arrange'                   => 'Sắp xếp',
    'btn.pending'                   => 'Chờ',
    'btn.publish'                   => 'Đăng',
    'btn.on'                        => 'Có',
    'btn.off'                       => 'Không',
    'btn.reset'                     => 'Đặt lại',
    'btn.send'                      => 'Gửi',
    'btn.view'                      => 'Chỉnh view',
    'btn.order'                     => 'Xếp hạng',

    'tag.live'                      => 'Hoạt động',
    'tag.blocked'                   => 'Khóa',

    /** User */
    'home'                          => 'Trang chủ',
    'contact'                       => 'Liên hệ quảng cáo',
    'sign_in'                       => 'Đăng Ký',
    'favorite_list'                 => 'Công thức yêu thích',
    'my_article'                    => 'Công thức đã đăng',
    'post_article'                  => 'Đăng công thức',
    'my_comment'                    => 'Bình luận của bạn',
    'other_comment'                 => 'Thông báo bình luận',
    'my_profile'                    => 'Trang cá nhân',
    'logout'                        => 'Thoát',

    /** Auth user */
    'lbl.sex_male'                  => 'Nam',
    'lbl.sex_female'                => 'Nữ',
    'lbl.sex_none'                  => 'Chọn giới tính',

    'btn.sign_in'                   => 'Đăng ký',
    'btn.sign_in_w_g'               => 'Đăng ký với Google',
    'btn.sign_in_w_f'               => 'Đăng ký với Facebook',
    'btn.log_in'                    => 'Đăng nhập',
    'btn.log_in_w_g'                => 'Đăng nhập với Google',
    'btn.log_in_w_f'                => 'Đăng nhập với Facebook',
    'available'                     => 'Khả dụng',
    'outdated'                      => 'Hết hạn',
];