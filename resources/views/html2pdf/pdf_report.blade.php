<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="{{ asset('assets/users/css/bootstrap.css') }}">

        <style>
            * {
                font-family: "DejaVu Sans" !important;
            }

            td.factor {
                border-bottom: 1px solid #ddd;
                border-right: 1px solid #ddd;
            }
            td.result{
                border-bottom: 1px solid #ddd;
                padding-left: 40px;

            }
            .iphone {
                position: relative;
                /*height: 100%;*/
                /*width: auto;*/
                width: 229px;
                height: 499px;
            }
            .get-content {
                position: absolute;
                left: 14.8%;
                top: 9%;
                height: 67.9%;
                width: 192px;
            }
            .seo-header{
                background-color: #6eb804;
                padding: 15px;
            }
            #mobileSnapshoot{
                position: relative;
                height: 500px;
                width: 400px;
            }
            #standard_seo{
                margin-bottom: 5%;
                margin-top: 10%;
            }
            #mobile_seo{
                margin-top: 5%;
            }
            #backlink_seo{
                margin-top: 5%;
            }
            .factor{
                text-align: center;
            }
           table{
               border: 1px solid #929292;
           }
            .logo-pdf{
                background-color: #b9b9b9;
                padding:15px;
            }
            .title-pdf{
                text-align: center;
                margin:15px auto;
                line-height: 0.5;
            }
            .cssTitle{
                border-top: 2px solid #929292;
                color:#B9160D;
            }
            .factor{
                font-weight: bold;
            }


        </style>
    </head>
    <body>
    <header>
        <div class="logo-pdf">
            <img src="{{ URL::asset('assets/images/logo-2.png')}}" alt="">
        </div>
        <div class="title-pdf">
            <h2>Kết quả phân tích SEO </h2>
            {!! $html_content['url']!!}
        </div>
    </header>
            <table id="standard_seo">
                <tbody>
                    <tr>
                        <td class="seo-header" colspan="2">TIÊU CHUẨN SEO</td>
                    </tr>
                    @foreach($html_content['factor']['standard'] as $index => $factor)
                        <tr>
                            <td class="factor">{!! $factor !!}</td>
                            <td class="result">
                                {!! $html_content['analytic_detail']['standard'][$index]['html'] !!}
                                @if($html_content['analytic_detail']['standard'][$index]['isPassed']=="false")
                                    <br><div class="cssTitle">{!! $html_content['analytic_detail']['standard'][$index]['modalTitle']!!}</div>
                                    {!! $html_content['analytic_detail']['standard'][$index]['modalBody'] !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        <table id="speed_seo">
                <tbody>
                    <tr>
                        <td class="seo-header" colspan="2">TỐC ĐỘ LOAD WEBPAGE</td>
                    </tr>
                    @foreach($html_content['factor']['speed'] as $index => $factor)
                        <tr>
                            <td class="factor">{!! $factor !!}</td>
                            <td class="result">
                                {!! $html_content['analytic_detail']['speed'][$index]['html'] !!}
                                @if($html_content['analytic_detail']['speed'][$index]['isPassed']=="false")
                                    <br><div class="cssTitle">{!! $html_content['analytic_detail']['speed'][$index]['modalTitle']!!}</div>
                                    {!! $html_content['analytic_detail']['speed'][$index]['modalBody'] !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
        </table>
            <table id="mobile_seo">
                <tbody>
                    <tr>
                        <td class="seo-header" colspan="2">DI ĐỘNG</td>
                    </tr>
                    @foreach($html_content['factor']['mobile'] as $index => $factor)
                        <tr>
                            <td class="factor">{!! $factor !!}</td>
                            <td class="result">
                                {!! $html_content['analytic_detail']['mobile'][$index]['html'] !!}
                                @if($html_content['analytic_detail']['mobile'][$index]['isPassed']=="false")
                                    <br><div class="cssTitle">{!! $html_content['analytic_detail']['mobile'][$index]['modalTitle'] !!}</div>
                                    {!! $html_content['analytic_detail']['mobile'][$index]['modalBody'] !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td class="factor">{!! $html_content['mobile_snapshoot']['name'] !!}</td>
                        <td id="mobileSnapshoot" class="result">
                            <img class="iphone" src="{{asset($html_content['mobile_snapshoot']['iphone'])}}">
                            <img class="get-content" id="picture-mobile" src="{{asset($html_content['mobile_snapshoot']['snapshoot'])}}">
                        </td>
                    </tr>
                </tbody>
            </table>
            <table id="backlink_seo">
                <tbody>
                <tr>
                    <td class="seo-header" colspan="2">SEO OFFPAGE</td>
                </tr>
                @foreach($html_content['factor']['backlink'] as $index => $factor)
                    <tr>
                        <td class="factor">{!! $factor !!}</td>
                        <td class="result">
                            {!! $html_content['analytic_detail']['backlink'][$index]['html'] !!}
                            @if($html_content['analytic_detail']['backlink'][$index]['isPassed']=="false")
                                <br><div class="cssTitle">{!! $html_content['analytic_detail']['backlink'][$index]['modalTitle']!!}</div>
                                {!! $html_content['analytic_detail']['backlink'][$index]['modalBody'] !!}
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
    </body>
</html>