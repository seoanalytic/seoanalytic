@extends('admin.layouts.master')
@section('content')
	<!-- Page header -->
    <div class="page-header page-header-default ">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Dashboard</span></h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="{{ url('admin/') }}"><i class="icon-home2 position-left"></i>Dashboard</a></li>
            </ul>
        </div>
    </div>
    <!-- /page header -->
    <!-- Content area -->
    <div class="content">
        <div class="row">
        	<div class="col-md-12">
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title text-bold"> <i class="icon-question3"></i> Câu hỏi mới nhất</h6>
                    </div>
                    <div class="table-responsive chat-stacked ">
                        <div class="media-list ">
                            <table class="table text-nowrap ">
                                <thead>
                                    <tr>
                                        <th>Người gửi</th>
                                        <th>Nội dung</th>
                                        <th>Trạng thái</th>
                                    </tr>
                                </thead>
                                <tbody >
                                   @foreach($data as $index=>$FAQ)
                                    <tr>
                                        <td>
                                            <div class="media-left media-middle">
                                                <a href="{!! URL::route('admin.faq.edit', $FAQ["id_questions"]) !!}">
                                                    <img src="{!! URL::asset('assets/images/question-faq-icon.png')!!}" class="img-circle" alt="">
                                                </a>
                                            </div>

                                            <div class="media-body">
                                                <div class="media-heading">
                                                    <a href="{!! URL::route('admin.faq.edit', $FAQ["id_questions"]) !!}" class="letter-icon-title">
                                                    {{ str_limit($FAQ["fullname"] , $limit = 20, $end = '...') }}</a>
                                                </div>
                                                <div class="text-muted text-size-small">{{ $FAQ["created_at"] }} </div>
                                            </div>
                                        </td>
                                        <td>
                                            {{ str_limit($FAQ->content_questions , $limit = 30, $end = '...') }}
                                        </td>
                                        <td>
                                            <h6 class="text-semibold no-margin">
                                                @if($FAQ->status_questions == 1)
                                                    <span class="label label-danger">Chưa Phản Hồi</span>
                                                @else
                                                    <span class="label label-success">Phản Hồi</span></a>
                                                @endif
                                            </h6>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('scripts')
    <!-- Core JS files -->
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/loaders/blockui.min.js') }}"></script>
	<!-- Theme JS files -->
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/ui/fullcalendar/fullcalendar.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/pages/timelines.js') }}"></script>
	<!-- /global stylesheets -->

@stop


