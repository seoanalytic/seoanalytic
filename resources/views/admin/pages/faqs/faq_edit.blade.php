@extends('admin.layouts.master')
@section('content')

<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Quản Lý Hỏi Đáp</span> - 
                Cập Nhật</h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/admin"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li><a href="/admin/hoi-dap">Quản Lý Hỏi Đáp</a></li>
            <li class="active">Cập nhật</li>
        </ul>
    </div>
</div>
<!-- /page header -->

<div class="content">
    
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Trả lời câu hỏi của khách hàng</h5>
        </div>
        
        <div class="panel-body">
            <form action="{{ route('admin.faq.email') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                <input type="hidden" name="id_questions" value="{{ $data['id_questions'] }}"/>
                <input type="hidden" name="status_questions_feedback" value="{{ $data['status_questions'] }}"/>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label class="text-bold">Tên khách hàng</label>
                        <input class="form-control" name="fullname" value="{{$data['fullname']}}" readonly>
                    </div>
                    <div class="form-group col-md-6">
                        <label class="text-bold">Email khách hàng</label>
                        <input class="form-control" name="email_questions" value="{{$data['email_questions']}}" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="text-bold">Nội dung câu hỏi</label>
                    <textarea class="form-control" rows="3" name="content_questions" readonly >{{ old('content_questions', isset($data)? $data['content_questions']: '') }}</textarea>
                </div>

                <div class="form-group  {{$errors->has('reply_questions') ? 'has-error' : '' }}">
                    <label class="text-bold">Phần trả lời</label> <span class="text-danger" title="Bắt buộc nhập" > *</span>
                    <textarea class="form-control" rows="3" name="reply_questions" >{{ old('reply_questions', isset($data)? $data['reply_questions']: '') }}</textarea>
                    @if ($errors->has('reply_questions'))
                        <span class="help-block">
                            <strong>{{ $errors->first('reply_questions') }}</strong>
                        </span>
                    @endif
                </div>
                
                <div class="text-right">
                    <a value="{!! route('admin.faq.index') !!}" class="btn btn-default btn-cancel" role="button">Hủy</a>
                    <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-send"></i> Gửi mail </button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
@section('scripts')
    <!-- Core JS files -->
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/libraries/bootstrap.min.js') }}"></script>

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/forms/styling/uniform.min.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/app.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/pages/swal_delete.js') }}"></script>
@stop