@extends('admin.layouts.master')
@section('content')

<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Quản Lý Hỏi Đáp</span></h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/admin"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li><a href="/admin/hoi-dap">Quản Lý Hỏi Đáp</a></li>
        </ul>
    </div>
</div>
<!-- /page header -->

<div class="content">
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Trả lời câu hỏi của khách hàng</h5>
        </div>
        
        <div class="panel-body">
        @if($msg['error'] == 0)
        	<div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
				<a href="{{ URL::route('admin.faq.index') }}"><span class="text-semibold">{{ $msg['msg'] }}</span> </a>
		    </div>
        @elseif($msg['error'] == 1)
            <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                <a href="{{ URL::route('admin.faq.index') }}"><span class="text-semibold">{{ $msg['msg'] }}</span> </a>
            </div>
        @endif
            
		    <div class="text-right">
                <a href="{{ URL::route('admin.faq.index') }}"><button type="submit" class="btn btn-success"><i class="icon-arrow-left8"></i> Quay lại </button></a>
            </div>
        </div>
    </div>
</div>
@stop
@section('scripts')
    <!-- Core JS files -->
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/libraries/bootstrap.min.js') }}"></script>

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/forms/styling/uniform.min.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/app.js') }}"></script>
@stop