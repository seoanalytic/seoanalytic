@extends('admin.layouts.master')
@section('content')

<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Quản Lý Hỏi Đáp</span> -
                Danh Sách</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin"><i class="icon-home2 position-left"></i> Dashboard</a></li>
                <li class="active">Quản Lý Hỏi Đáp</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->

    <div class="content">
        <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Danh sách câu hỏi của khách hàng</h5>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered datatable-save-state">
                    <thead>
                    <tr class="bg-green-600">
                        <th>STT</th>
                        <th>Người gửi</th>
                        <th style="width: 15%">Nội dung câu hỏi</th>
                        <th style="width: 15%">Nội dung trả lời</th>
                        <th>Người trả lời</th>
                        <th>Tình trạng</th>
                        <th class="text-center">Hành động</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $index => $item)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td align="center">
                                <span style="font-weight: bold; font-size: 15px;">{{ $item["fullname"] }}</span><br/>
                                {{ $item["created_at"] }}
                            </td>
                            <td align="left">{{ str_limit($item["content_questions"], $limit = 50, $end = '...') }}</td>
                            <td align="left">{{ str_limit($item["reply_questions"], $limit = 50, $end = '...') }}</td>
                            <td align="left">
                                @if ($item["id_user"]=='')

                                @else
                                    <?php
                                    $user_post = DB::table('users')->where('id', $item["id_user"])->first();
                                    echo $user_post->name;
                                    ?>
                                @endif
                            </td>
                            <td>
                                @if($item["status_questions"] == 1)
                                    <span class="label label-danger">Chưa Phản Hồi</span>
                                @else
                                    <span class="label label-success">Phản Hồi</span></a><br/>
                                    {{ $item["updated_at"] }}
                                @endif
                            </td>
                            <td>
                                <ul class="icons-list">
                                    <li>
                                        <a value="{{ URL::route('admin.faq.del', $item["id_questions"]) }}" class="btn-delete-swal"
                                        data-title="@lang('messages.title_delete', ['name_object' => __('messages.faq')])"
                                        data-text="@lang('messages.text_delete', ['name_object' => __('messages.faq')])"><i class="icon-trash btn text-danger-600"></i></a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::route('admin.faq.edit', $item["id_questions"]) }}"><i class="icon-pencil7 btn text-primary-600"></i></a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @stop
    @section('scripts')
    <!-- Core JS files -->
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/libraries/bootstrap.min.js') }}"></script>

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/pages/datatables_basic.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/notifications/bootbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/notifications/sweet_alert.min.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/pages/swal_delete.js') }}"></script>
    @stop