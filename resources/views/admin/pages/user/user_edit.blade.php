@extends('admin.layouts.master')
@section('content')

<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Quản Lý Người Dùng</span> - 
                Cập Nhật</h4>
        </div>
    </div>

    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/admin"><i class="icon-home2 position-left"></i> Dashboard</a></li>
            <li><a href="/admin/nguoi-dung">Quản Lý Người Dùng</a></li>
            <li class="active">Cập nhật</li>
        </ul>
    </div>
</div>
<!-- /page header -->

<div class="content">
    
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Thông tin người dùng</h5>
        </div>
        
        <div class="panel-body">
            <form id="form_admin_update_profile" action="{{route('admin.user.updateProfile', $data['id_users'])}}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                <input type="hidden" name="id_users" value="{{ $data['id_users'] }}"/>
                <div class="row">
                    <div class="group-input form-group col-md-6" id="admin_user_name">
                        <label class="text-bold">Tên người dùng</label>
                        <input class="form-control" name="admin_user_name" value="{{$data['name']}}" readonly>
                        <strong><span class="help-block"> </span></strong>
                    </div>
                    <div class="group-input form-group col-md-6" id="admin_user_email">
                        <label class="text-bold">Email người dùng</label>
                        <input class="form-control" name="admin_user_email" value="{{$data['email']}}" readonly>
                        <strong><span class="help-block"> </span></strong>
                    </div>
                </div>
                <div class="row">
                    <div class="group-input form-group col-md-6">
                        <label class="text-bold">Mật khẩu</label>
                        <input class="form-control" value="{{$data['password']}}" readonly>
                        <strong><span class="help-block"> </span></strong>
                    </div>
                </div>
                <div class="row">
                    <div class="group-input form-group col-md-12">
                        <label class="text-bold">Lịch sử tra cứu</label>
                        <textarea class="form-control" rows="3" readonly>{{ old('history', isset($data['history'])? $data['history']: 'Chưa có lịch sử tra cứu') }}</textarea>
                        <strong><span class="help-block"> </span></strong>
                    </div>
                </div>
                <div class="row">
                    <div class="group-input form-group col-md-6" id="admin_user_created_at">
                        <label class="text-bold">Ngày khởi tạo</label>
                        <input class="form-control" name="admin_user_created_at" value="{{$data['created_at']}}" readonly>
                        <strong><span class="help-block"> </span></strong>
                    </div>
                    <div class="group-input form-group col-md-6" id="admin_user_updated_at">
                        <label class="text-bold">Ngày cập nhật lần cuối</label>
                        <input class="form-control" name="admin_user_updated_at" value="{{$data['updated_at']}}" readonly>
                        <strong><span class="help-block"> </span></strong>
                    </div>
                </div>
              
                
                <div class="text-right">
                    <a value="{!! route('admin.user.index') !!}" class="btn btn-default btn-cancel" role="button">Hủy</a>
                    <!-- <button id="btn_admin_update_profile" type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-ok"></i> Cập nhật thông tin </button> -->
                </div>
            </form>
        </div>
    </div>
</div>
@stop
@section('scripts')
    <!-- Core JS files -->
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/libraries/bootstrap.min.js') }}"></script>

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/forms/styling/uniform.min.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/app.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/pages/swal_delete.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/pages/edit_user.js') }}"></script>
@stop