@extends('admin.layouts.master')
@section('content')

<!-- Page header -->
<div class="page-header page-header-default">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Quản Lý Người Dùng</span> -
                Danh Sách</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin"><i class="icon-home2 position-left"></i> Dashboard</a></li>
                <li class="active">Quản Lý Người Dùng</li>
            </ul>
        </div>
    </div>
    <!-- /page header -->

    <div class="content">
        <!-- Basic datatable -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Danh sách người dùng</h5>
            </div>

            <div class="table-responsive">
                <table class="table table-bordered datatable-save-state">
                    <thead>
                    <tr class="bg-green-600">
                        <th>STT</th>
                        <th>Tên người dùng</th>
                        <th style="width: 15%">Email</th>
                        <th style="width: 5%">Mật khẩu</th>
                        <th>Ngày Tạo</th>
                        <th>Lần chỉnh sửa cuối</th>
                        <th class="text-center">Hành động</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $index => $item)
                    
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td align="left">{{ str_limit($item["name"], $limit = 50, $end = '...') }}</td>
                            <td align="left">{{ str_limit($item["email"], $limit = 50, $end = '...') }}</td>
                            <td align="left" style="width: 5%">{{ substr($item["password"],0,25).'...' }}</td>
                            <td>{{ $item["created_at"] }}</td>
                            <td>{{ $item["updated_at"] }}</td>
                            <td>
                                <ul class="icons-list">
                                    <li>
                                        <a value="{{ URL::route('admin.user.del', $item["id_users"]) }}" class="btn-delete-swal"
                                        data-title="@lang('messages.title_delete', ['name_object' => __('messages.user')])"
                                        data-text="@lang('messages.text_delete', ['name_object' => __('messages.user')])"><i class="icon-trash btn text-danger-600"></i></a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::route('admin.user.edit', $item["id_users"]) }}"><i class="icon-pencil7 btn text-primary-600"></i></a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @stop
    @section('scripts')
    <!-- Core JS files -->
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/libraries/bootstrap.min.js') }}"></script>

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/pages/datatables_basic.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/notifications/bootbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/notifications/sweet_alert.min.js') }}"></script>

    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/pages/swal_delete.js') }}"></script>
    @stop