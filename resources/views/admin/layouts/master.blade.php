<!DOCTYPE html>
<html lang="en">
<head>
	<link href="{{ asset('favicon.ico') }}" rel="shortcut icon" type="image/x-icon"/>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="_token" content="{{ csrf_token() }}"/>
	<title>SEO Analytics</title>

	@include('admin.templates.head')
</head>

<body>
	@include('admin.templates.header')

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			@include('admin.templates.sidebar')

			<!-- Main content -->
			<div class="content-wrapper">
				@yield('content')
			</div>
			<!-- /main content -->
		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->

	@yield('scripts')
</body>
</html>
