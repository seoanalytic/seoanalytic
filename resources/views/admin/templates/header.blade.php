<!--Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="#"><img src="{{ URL::asset('assets/images/logo-1.png')}}" alt=""></a>
		</div>
		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
			<div class="navbar-right">
		        <ul class="nav navbar-nav">
		            <li><a><i class="icon-user"></i> {{ Auth::guard()->user()->name }}</a></li>
		            <li><a href="{{ url('admin/logout') }}"><i class="icon-switch2"></i> Logout</a></li>
		        </ul>           
		    </div>
		</div>
	</div>
<!-- /main navbar