<!-- Main sidebar -->
    <div class="sidebar sidebar-main">
        <div class="sidebar-content">

            <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img src="https://robohash.org/{{ Auth::user()->name }}.png?set=set1&size=200x200" class="img-circle" alt="" style="width: 50px; height: 50px"></a>
                            <div class="media-body">
                                <span class="media-heading text-semibold" style="margin-top: 12px!important;">{{ Auth::user()->name }}</span>
                                <div class="text-size-mini text-muted">
                                    Quản trị viên 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- /user menu -->


            <!-- Main navigation -->
            <div class="sidebar-category sidebar-category-visible">
                <div class="category-content no-padding">
                    <ul class="navigation navigation-main navigation-accordion">

                        <!-- Main -->
                        <!-- <li class="navigation-header"><span>SEO Main</span> <i class="icon-menu" title="Main pages"></i></li> -->
                        <li><a href="{{route('admin.home.index')}}"><i class="icon-home4"></i><span>Dashboard</span></a></li>
                        <li><a href="{{route('admin.user.index')}}"><i class="icon-user"></i> <span>Quản Lý Người dùng</span></a></li>
                        <li><a href="{{route('admin.faq.index')}}"><i class="icon-question3"></i> <span>Quản Lý FAQs</span></a></li>

                        <hr style="border: 1px dashed #ccc;" />
                        <li><a href="{{ url('admin/logout') }}"><i class="icon-switch2"></i> Logout</a></li>

                        <!-- /main -->
                    </ul>
                </div>
            </div>
            <!-- /main navigation -->

        </div>
    </div>
<!-- /main sidebar -->