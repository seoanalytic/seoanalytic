@extends('errors.layout')
@section('error')
    <h1>500</h1>
    <h2>Internal Server Error</h2>

    <p>Rất tiếc, đã xảy ra lỗi. Website bị quá tải!</p>

    <p><a href="javascript:history.go(-1)" class="btn btn-style">Quay về trang trước</a> <a href="/" class="btn btn-style">Về trang chủ</a></p>
@stop