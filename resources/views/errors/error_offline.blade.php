@extends('errors.layout')
@section('error')
    <h1>Offline</h1>

    <p>Rất tiếc, đã xảy ra lỗi. Website đang bảo trì!</p>

    <p><a href="javascript:history.go(-1)" class="btn btn-style">Quay về trang trước</a> <a href="/" class="btn btn-style">Về trang chủ</a></p>
@stop