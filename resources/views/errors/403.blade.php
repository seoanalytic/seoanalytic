@extends('errors.layout')

@section('error')
	<h1>403</h1>
	<h2>Forbidden</h2>

	<p>Rất tiếc, đã xảy ra lỗi. Bạn không đủ quyền để truy cập!</p>

	<p><a href="javascript:history.go(-1)" class="btn btn-style">Quay về trang trước</a> <a href="/" class="btn btn-style">Về trang chủ</a></p>
@stop