@extends('errors.layout')

@section('errror')
    <h1>503</h1>
    <h2>Service Unavailable</h2>

    <p>Rất tiếc, đã xảy ra lỗi. Website đang bảo trì!</p>

    <p><a href="javascript:history.go(-1)" class="btn btn-style">Quay về trang trước</a> <a href="/" class="btn btn-style">Về trang chủ</a></p>
@stop