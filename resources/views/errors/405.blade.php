@extends('errors.layout')

@section('error')
    <h1>405</h1> 
    <h2>Not Allowed</h2>

    <p>Rất tiếc, đã xảy ra lỗi. Yêu cầu không hợp lệ!</p>

    <p><a href="javascript:history.go(-1)" class="btn btn-style">Quay về trang trước</a> <a href="/" class="btn btn-style">Về trang chủ</a></p>
@stop
