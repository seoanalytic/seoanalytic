@extends('errors.layout')

@section('error')
	<h1>404</h1>
	<h2>Page Not Found</h2>

	<p>Rất tiếc, đã xảy ra lỗi. Không tìm thấy trang!</p>

	<p><a href="javascript:history.go(-1)" class="btn btn-style">Quay về trang trước</a> <a href="/" class="btn btn-style">Về trang chủ</a></p>
@stop
