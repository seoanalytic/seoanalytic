<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <p>Chào {{ $fullname }},</p>
    <p>Cảm ơn bạn đã nhắn tin cho chúng tôi</p>
    <p><strong>Nội dung câu hỏi của bạn:</strong> {{$content_questions}}</p>
    <p><strong>Câu trả lời dành cho bạn:</strong> {{$reply_questions}}</p>
    Thân ái,<br>{{ config('app.name') }}
</body>
</html>