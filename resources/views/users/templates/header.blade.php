<!-- header -->
	<input type="hidden" id="url_logout" value="{{route('users.logout')}}" >
    <input type="hidden" id="url_lookup_history" value="{{route('users.lookup_history')}}" >
    <input type="hidden" id="url_profile" value="{{route('users.profile')}}" >
    <div id="header">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">

            <div class="container">

                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{route('users.index')}}" alt=""><img src="{{ URL::asset('assets/images/logo-2.png')}}" alt=""></a>

                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="{{route('users.index')}}">Trang chủ</a>
                        </li>
						<?php $user = session('user'); ?>
                        @if($user == null)
                        <li class="not_login">
                            <a href="#" data-toggle="modal" data-target="#toolbox-login-modal"></span>Đăng nhập</a>
                        </li>
                        <!--<a href="" class="btn navbar-btn navbar-right btn-style">Đăng kí</a>-->
                        <li class="not_login">
                            <button type="button" class="btn btn-border btn-lg" data-toggle="modal" data-target="#toolbox-register-modal">Đăng Kí</button>
                        </li>
                        @else
                        <li class="do_login dropdown">
                        	<a class="dropdown-toggle do_login_toggle" data-toggle="dropdown">
                                <img src="http://i.pravatar.cc/100?img={{$user->id_users %70}}" class="img-circle" alt="">
                                <span class="text">{{$user->name}}</span>
                                <div class="btn-dropdown"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                            </a>
                        	<div class="dropdown-menu">
                        		<ul>
                                    <li> <a href="{{route('users.profile')}}">Thông tin cá nhân</a> </li>
                                    <li role="separator" class="divider"></li>
                        			<li> <a href="{{route('users.lookup_history')}}">Lịch sử tra cứu</a> </li>
                                    <li role="separator" class="divider"></li>
                                    <li> <a href="{{route('users.logout')}}">Đăng xuất</a> </li>
                        		</ul>
                        	</div>
                        </li>
                        @endif

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>

        </nav>
    </div>
<!-- /header -->

    <!-- start toolbox-login-modal -->
    <div class="modal fade login-register-modal" id="toolbox-login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-vertical-alignment-helper">
            <div class="modal-dialog modal-vertical-align-center">
                <div class="modal-content">
                    <div class="modal-body">

                        <div class="alert alert-danger col-md-12 hide">
                            <span class="alert-body-message"></span>
                        </div>

                        <form id="form_login" action="{{route('users.login')}}" class="row toolbox-login-modal-form" method="POST">
                        	<input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
                            <div class="col-md-12 register-form">
                                <div class="form-container">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h3>Đăng nhập vào tài khoản của bạn</h3>

                                    <div class="input-group" id="login_email">
                                        <label class="label-icon medium" for="email"><span class="icon email" aria-hidden="true"></span></label>
                                        <input name="login_email" type="email" class="form-control input-md has-label-icon required valid-email" id="login_email" placeholder="Địa chỉ Email" spellcheck="false" autofocus="autofocus">
                                        <strong><span class="help-block"> </span></strong>
                                    </div>

                                    <div class="input-group" id="login_password">
                                        <label class="label-icon medium" for="password"><span class="icon lock" aria-hidden="true"></span></label>
                                        <input name="login_password" type="password" class="form-control input-md has-label-icon required match-passwords" id="login_password" placeholder="Mật Khẩu">
                                        <strong><span class="help-block"> </span></strong>
                                    </div>

                                    <button type="submit" class="btn cta btn-md submit-button" id="btn_login" button-action="login">Đăng nhập</button>

                                    {{--<a href="#" class="pull-left switch-modal-to-change-password">Quên mật khẩu?</a>--}}
                                </div>
                            </div>
                        </form>

                        {{--<form class="row toolbox-change-password-modal-form hide">
                        	<input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
                            <div class="col-md-12 register-form">
                                <div class="form-container">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h3>Thay đổi mật khẩu</h3>

                                    <div class="input-group">
                                        <label class="label-icon medium" for="email"><span class="icon email" aria-hidden="true"></span></label>
                                        <input name="email" type="email" class="form-control input-md has-label-icon required valid-email" id="email" placeholder="Địa chỉ Email">
                                        <strong><span class="help-block"> </span></strong>
                                    </div>

                                    <button type="button" class="btn cta btn-md submit-button" button-action="change-password-request">Gửi yêu cầu</button>

                                    <a href="#" class="pull-left switch-modal-to-login">Đăng nhập vào tài khoản của bạn</a>
                                </div>
                            </div>
                        </form>--}}

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- end toolbox-login-modal -->

    <!-- modal reset pass -->

    
    
    <!-- /modal reset pass -->

    <!-- start toolbox-register-modal -->
    <div class="modal fade login-register-modal" id="toolbox-register-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-vertical-alignment-helper">
            <div class="modal-dialog modal-vertical-align-center">
                <div class="modal-content">
                    <div class="modal-body">

                        <div class="alert alert-danger col-md-12 hide">
                            <span class="alert-body-message"></span>
                        </div>

                        <form id="form_register" action="{{route('users.register')}}" method="POST" class="row toolbox-register-modal-form" data-label="register-form">
                        	<input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
                            <div class="col-md-12 register-form">
                                <div class="form-container">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h3>Đăng kí ngay bây giờ để nhận tính năng mới</h3>
									<div class="input-group" id="name">
                                        <label class="label-icon medium"><span class="icon user" aria-hidden="true"></span></label>
                                        <input name="name" type="text" class="form-control input-md has-label-icon required match-passwords"  placeholder="Họ và tên">
                                        <strong><span class="help-block"> </span></strong>
                                    </div>

									<div class="input-group" id="email">
                                        <label class="label-icon medium"><span class="icon email" aria-hidden="true"></span></label>
                                        <input name="email" type="email" class="form-control input-md has-label-icon required match-passwords"  placeholder="Địa chỉ email">
                                        <strong><span class="help-block"> </span></strong>
                                    </div>

                                    <div class="input-group" id="password">
                                        <label class="label-icon medium"><span class="icon lock" aria-hidden="true"></span></label>
                                        <input name="password" type="password" class="form-control input-md has-label-icon required match-passwords"  placeholder="Mật khẩu">
                                        <strong><span class="help-block"> </span></strong>
                                    </div>

                                    <div class="input-group" id="repeatpass">
                                        <label class="label-icon medium"><span class="icon lock" aria-hidden="true"></span></label>
                                        <input name="repeatpass" type="password" class="form-control input-md has-label-icon required match-passwords"  placeholder="Nhập lại mật khẩu">
                                        <strong><span class="help-block"> </span></strong>
                                    </div>

                                    <button type="submit" id="btn_register" class="btn cta btn-md submit-button" button-action="register">Tạo tài khoản</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end toolbox-register-modal -->