<section class="footer black">
    <!-- start container -->
    <div class="container">

        <!-- start row -->
        <div class="row">

            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 footer-column">
                <h4>Giới thiệu</h4>
                <p style="color: #fff;">Website đưa ra nhận xét, đánh giá, so sánh website của bạn và của đối thủ cạnh tranh dựa trên các tiêu chí của SEO.</p>
            </div>
            
            <!-- start column -->
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 footer-column">
                <h4>Liên kết</h4>
                    <ul class="social">
                        <li>
                            <a href=""> <i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href=""> <i class="fa fa-twitter" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                        </li>
                        <li>
                            <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                        </li>
                     </ul>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 footer-column">
                <h4>Hỗ trợ</h4>
            </div>
            <!-- end column -->

        </div>

        <!-- end row -->
      <!-- footer -->
    
    <!-- /footer -->
    </div>
    <!-- end container -->
    <div class="container contact">
        <div class="row">
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 contact1">
                <div class="contact-title main-item">
                    <div class="contact-item">
                        <i class="slz-icon fa fa-map-marker"></i>
                        <div class="text">Địa chỉ</div>
                    </div>
                 </div>
                 <div class="contact-content sub-item">
                    <div class="text">KTX Khu B - ĐHQG TP.HCM</div>
                </div>
            
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 contact1">
                <div class="contact-title main-item">
                    <div class="contact-item">
                        <i class="slz-icon fa fa-phone"></i>
                        <div class="text">Điện thoại</div>
                    </div>
                 </div>
                 <div class="contact-content sub-item">
                    <div class="text">0985442809 - 01232502601</div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 contact1">
                <div class="contact-title main-item">
                    <div class="contact-item">
                        <i class="slz-icon fa fa-envelope"></i>
                        <div class="text">Email</div>
                    </div>
                 </div>
                 <div class="contact-content sub-item">
                    <div class="text">example@gmail.com</div>
                </div>
            </div>
        </div>
    </div>
    <div class="last ">
        <div class="container">
            <div class="copy">Copyright © <i class="fa fa-heart text-primary"></i> Website SEO 2017 </div>
        </div>
    </div>
</section>

  