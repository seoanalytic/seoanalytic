@extends('users.layouts.master')

@section('content')
<!-- content -->
  
    <section id="history_form">
        <div class="intro">
            <h2 class="h2">Tạo Favicon</h2>
        </div>
        <div class="panel panel-success">
            <div class="panel-heading">Chọn hình ảnh tạo Favicon</div>
            	<div class="panel-body"> 
		        	<form class="col-md-6 col-sm-6 col-xs-12" action="{{ route('tool.postfavicon') }}"  enctype="multipart/form-data" method="post">
						{{ csrf_field() }}
						<p><strong>Chọn kích thước:</strong><br />
							<select class="form-control" style="width:170px" name="image_dimensions">
								<option value="16">16px x 16px</option>
								<option value="32">32px x 32px</option>
							</select>
						</p>
						<p><strong>Chọn ảnh:</strong>
							<input class="form-control" name="image" size="40" type="file" />
						</p>
						<input name="submit" type="submit" value="Tạo Favicon" class="btn btn-green"/>
					</form>
					@if(!empty($generate_favicon))
						@if($generate_favicon['code'] == 1)
							<div class="col-md-6 col-sm-6 col-xs-12">
								<strong>Xem trước:</strong><br/>
								<img src="/favicon/{{$generate_favicon['msg']}}.ico" border="0" style="padding: 4px 0px 4px 0px;background-color:#e0e0e0" /><br/>
								Tạo Favicon thành công! <br/><a download="favicon" class="btn btn-green" href="/favicon/{{$generate_favicon['msg']}}.ico" target="_blank">Download</a><br/>
								Gắn mã sau đây trong thẻ head HTML của bạn:
								<div class="bg-error">&lt;link rel=&quot;shortcut icon&quot; type=&quot;image/x-icon&quot; href=&quot;./favicon.ico&quot;&gt;</div>
							</div>
							
						@else
							<div class="col-md-6 col-sm-6 col-xs-12 bg-error">
								<i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>
								{{$generate_favicon['msg']}}
							</div>
						@endif	
					@endif	
				</div>
			</div>
        </div>
		
		
    </section>

<!-- /content -->
@endsection

@section('page_scripts')

@endsection