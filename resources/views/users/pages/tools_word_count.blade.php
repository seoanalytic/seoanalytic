@extends('users.layouts.master')

@section('content')
<!-- content -->
    
    <section id="history_form">
        <div class="intro">
            <h2 class="h2">Công cụ đếm Từ &amp; Ký tự</h2>
        </div>
        <form>
            <div class="form-group" style="margin:0px;">
                <label>Nhập text vào ô bên dưới để biết được đoạn text của bạn có bao nhiêu từ &amp; ký tự.</label>
                <textarea id="text" cols="80" rows="12" class="form-control" style="height: 200px;"></textarea>
            </div>
            <div class="panel panel-success">
                <div class="panel-heading text-center" style="font-size:20px;">
                    <div id="counter"> Kết quả:
                        <span id="wordCount" style="color: #f00; font-weight: bold;">0</span> từ</span> &amp;
                        <span id="charCount" style="color: #f00; font-weight: bold;">0</span> ký tự 
                    </div>
                </div>
            </div>
        </form>
    </section>

<!-- /content -->
@endsection

@section('page_scripts')

    <script type="text/javascript">
        counter = function() {
            var value = $('#text').val();

            if (value.length == 0) {
                $('#wordCount').html(0);
                $('#charCount').html(0);
                return;
            }
            var regex = /\s+/gi;
            var wordCount = value.trim().replace(regex, ' ').split(' ').length;
            var charCount = value.trim().length;

            $('#wordCount').html(wordCount);
            $('#charCount').html(charCount);
        };
        $(document).ready(function() {
            $('#text').change(counter);
            $('#text').keydown(counter);
            $('#text').keypress(counter);
            $('#text').keyup(counter);
            $('#text').blur(counter);
            $('#text').focus(counter);
        });
    </script>

@endsection