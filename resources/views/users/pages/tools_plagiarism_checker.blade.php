@extends('users.layouts.master')

@section('content')
<!-- content -->
    
    <section id="history_form">
        <div class="intro">
            <h2 class="h2">Công cụ Plagiarism Checker</h2>
        </div>
        <form action>
            <div class="form-group fm-checker">
                <label>Nhập text Kiểm tra</label>
                <textarea id="text" cols="80" rows="12" class="form-control" style="height: 200px;"></textarea>
            </div>
            <button type="submit" class="btn btn-green btn-check">Kiểm tra</button>
        </form>
    </section>

<!-- /content -->
@endsection

@section('page_scripts')

@endsection