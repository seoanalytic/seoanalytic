@extends('users.layouts.master')

@section('content')
	
	<!-- menu result analytics -->
	@include('users.partials.result.menu')

	<!-- statistics -->
	@include('users.partials.result.statistics')
	<div id="HTML-TO-PDF">
	<!-- standard_seo -->
	@include('users.partials.result.standard_seo')

	<!-- speed -->
	@include('users.partials.result.speed')

	<!-- mobile -->
	@include('users.partials.result.mobile')
	</div>


@endsection