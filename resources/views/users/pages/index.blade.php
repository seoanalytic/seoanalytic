@extends('users.layouts.master')

@section('content')
<!-- content -->
    
    <div id="content">
        <section class="main-banner">
            <div class="container">
                <form action="{{route('check.index')}}" class="row analysis-checkup" method="get">
                    <!-- start form group -->
                    <div class="form-group align-to-banner">
                        <h1>Công Cụ Phân Tích SEO</h1>
                        <h2 id="changeText"></h2>
                        <div class="check-url">
                            <div class="group-input">
                                <i class="fa fa-link" aria-hidden="true"></i>
                                <input name="url" type="url" spellcheck="false" autocomplete="on" class="valid-url" id="select-url" placeholder="Đường dẫn của Website (http://)">
                            </div>
                            <!-- end select url -->
                            <button type="submit" class="btn" button-action="checkup"><span>Kiểm tra!</span></button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
    <!-- giới thiệu -->
    @include('users.partials.home.introduce')
    <!-- tools hỗ trợ -->
    @include('users.partials.home.tools')
    <!-- demo video sản phẩm -->
    @include('users.partials.home.demo_product')
    <!-- định nghĩa seo -->
    @include('users.partials.home.define_seo')  
    <!-- nhóm chúng tôi -->
    @include('users.partials.home.about_us') 
    <!-- contact -->
    @include('users.partials.home.contact')


<!-- /content -->
@endsection

@section('page_scripts')

    <script type="text/javascript">
        var text = ["Phân tích tình trạng website của bạn",
                    "Đưa ra giải pháp tối ưu cho website"];
        var count = 0;
        var elem = document.getElementById("changeText");
        setInterval(change, 2000);
        function change() {
            elem.innerHTML = text[count];
            count++;
            if(count >= text.length) { count = 0; }
        };
    </script>

@endsection