@extends('users.layouts.master')

@section('content')
    
    <section id="history_form">
        <div class="intro">
            <h2 class="h2">Lịch sử tra cứu</h2>
        </div>
        @if ($lookup_history!=NULL)
            <div>
            @foreach ($lookup_history as $index => $lookup_record)
            <a class="link" href="/du-lieu?url={{urlencode($lookup_record->url)}}&compare_id={{$lookup_record->id}}" target="_blank">
                <div class="hf clearfix">
                    <div class="stt col-md-2">{{$index+1}}</div>
                    <div class="col-md-6 text-align-left">
                         {{$lookup_record->url}}   
                    </div>
                    <div class="col-md-4">
                        <p>{{date('H:i:s d-m-Y', strtotime($lookup_record->created_at))}}</p>
                    </div>
                </div>
            </a> 
            @endforeach
            </div>
        @else
            <div class="no_hf">
                Bạn không có lịch sử tra cứu nào
            </div>
        @endif
    </section>
    
@endsection
