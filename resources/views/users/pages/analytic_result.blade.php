@extends('users.layouts.master')

@section('content')
	
	<!-- menu result analytics -->
	@include('users.partials.result.menu')

	<!-- statistics -->
	@include('users.partials.result.statistics')

	<!-- standard_seo -->
	@include('users.partials.result.standard_seo')

	<!-- speed -->
	@include('users.partials.result.speed')

	<!-- mobile -->
	@include('users.partials.result.mobile')

	<!-- backlink -->
	@include('users.partials.result.backlink')

@endsection

@section('page_scripts')
	
	<script type="text/javascript">
		@if($isCompare)
		    var old_html_content = {!!$html_content!!};

			function displayOldSeoData()
			{
				var date_in_past = '<p><b>Thông tin cũ tại thời điểm: {{$date_in_past}}</b></p>';
			    $('#mobileSnapshoot').append('<div class="old-seo-data"><img class="get-content old-mobile-snapshoot" id="picture-mobile-old" src="'+old_html_content.mobile_snapshoot.snapshoot+'"/><img  class="iphone old-background-iphone" src="/mobile-snapshoot/iphone5s.png" /></div>');
				$('.analytic-detail.standard_seo').each(function(index){
					// console.log(old_html_content.analytic_detail.standard[index])
					$(this).append('<hr class="bs-docs-separator"><div class="old-seo-data">'+date_in_past+old_html_content.analytic_detail.standard[index].html+'</div>');
			    });
			    $('.analytic-detail.speed_seo').each(function(index){
			    	// console.log(old_html_content.analytic_detail.speed[index])
			        $(this).append('<hr class="bs-docs-separator"><div class="old-seo-data">'+date_in_past+old_html_content.analytic_detail.speed[index].html+'</div>');
			    });
			    $('.analytic-detail.mobile_seo').each(function(index){
			    	// console.log(old_html_content.analytic_detail.mobile[index])
			        $(this).append('<hr class="bs-docs-separator"><div class="old-seo-data">'+date_in_past+old_html_content.analytic_detail.mobile[index].html+'</div>');
			    });

			}
	    @endif

		/* meta keywword */
		if (document.getElementById("meta-keywords").innerText == "")
			document.getElementById("meta-keywords").innerHTML = '0 keyword.';

		/* meta description */
		if (document.getElementById("meta-description").innerText == ""){
			document.getElementById("icon-description").innerHTML = '<i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>';
			document.getElementById("meta-description").innerHTML = '0 description. <br/><button type="button" class="mar-t btn-sweet" data-toggle="modal" data-target="#description">Khắc phục</button><div class="modal fade" id="description" role="dialog"><div class="modal-vertical-alignment-helper"><div class="modal-dialog modal-vertical-align-center"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"><b>Hướng dẫn khắc phục Meta description</b></h4></div><div class="modal-body"><p>Meta description đóng vai trò như một lời giới thiệu với mọi người về trang web để giải quyết vấn đề này, bạn nên:</p><ul><li>Meta description phải có chứa từ khóa.</li><li>Chiều dài tối ưu cho meta description nằm trong khoảng từ 70 đến 160 ký tự (khoảng 25 - 30 từ).</li></ul></div><div class="modal-footer"><button type="button" class="btn btn-default button-close" data-dismiss="modal">Đóng</button></div></div></div></div></div>';
		}

		/* canonical */
		var element = document.createElement("a");
		var hrefa = document.getElementById("href-can");
		if (typeof(hrefa) != 'undefined' && hrefa != null){
			hrefa = hrefa.innerHTML;
			document.getElementById("icon-can").innerHTML = '<i class="fa fa-check-circle-o" aria-hidden="true" data-toggle="popover" data-content="OK"></i>';
			element.href = hrefa;
			element.innerHTML = hrefa;
			document.getElementById("canonical").innerHTML = 'Trang của bạn có sử dụng thẻ canonical.<br/>';
			document.getElementById("canonical").appendChild(element);
		}
		else{
			document.getElementById("icon-can").innerHTML = '<i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>';
			document.getElementById("canonical").innerHTML = 'Trang của bạn không có sử dụng thẻ canonical.<br/><button type="button" class="mar-t btn-sweet" data-toggle="modal" data-target="#warn-can">Khắc phục</button><div class="modal fade" id="warn-can" role="dialog"><div class="modal-vertical-alignment-helper"><div class="modal-dialog modal-vertical-align-center"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button><h4 class="modal-title"><b>Hướng dẫn sử dụng thẻ Canonical</b></h4></div><div class="modal-body"><p> Thẻ canonical sẽ giúp cho Google biết trang website của bạn không phải một bản sao và nó nên được tìm thấy trong kết quả tìm kiếm website</p><ul><li>Sử dụng thẻ canonical cho danh mục sản phẩm.</li><li>Sử dụng canonical khi bạn có đường dẫn đã tối ưu, và đường dẫn chưa tối ưu đã được index.</li><li>Không nên sử dụng canonical cho phân trang của danh mục, bài viết.</li></ul></div><div class="modal-footer"><button type="button" class="btn btn-default button-close" data-dismiss="modal">Đóng</button></div></div></div></div></div>';
		}
		

		/* favicon */
		var elem = document.createElement("img");
		var hrefImg = document.getElementById("img-favicon");
		if (typeof(hrefImg) != 'undefined' && hrefImg != null)
		{
			hrefImg = hrefImg.innerHTML;
			if(hrefImg.indexOf("http://") == -1  && hrefImg.indexOf("https://") == -1){
				if(hrefImg.indexOf(".com") == -1){
					if(hrefImg.substring(0, 1) != '/'){
						hrefImg = '<?php echo parse_url($domain)["scheme"];?>'+ '://' + '<?php echo parse_url($domain, PHP_URL_HOST);?>'+ '/' + hrefImg;
					}
					else hrefImg = '<?php echo parse_url($domain)["scheme"];?>'+ '://' + '<?php echo parse_url($domain, PHP_URL_HOST);?>' + hrefImg;
				}
				else 
					hrefImg = '<?php echo parse_url($domain)["scheme"];?>'+ '://' + hrefImg;
			}
			
			document.getElementById("icon-favicon").innerHTML = '<i class="fa fa-check-circle-o" aria-hidden="true" data-toggle="popover" data-content="OK"></i>';
			elem.src = hrefImg;
		   	elem.setAttribute("width", "32");
			elem.setAttribute("height", "32");

			document.getElementById("content-favicon").innerHTML = "Chúc mừng! Trang web có favicon.<br/>";
			document.getElementById("content-favicon").appendChild(elem);
		}
		else{
			document.getElementById("icon-favicon").innerHTML = '<i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>';
			document.getElementById("content-favicon").innerHTML = 'Trang web không có favicon.<br/><form action="/tao-favicon" method="get"><button class="mar-t btn-sweet">Tạo Favicon</button></form>';
		}

		/* view/less heading */
		$(document).ready(function () {
		    var height = $(".view-h").find('div').height();
		    if(height <= $(".view-h").height())
		    {
		        $('.button-view-less').hide();
		    }
		    else 
		    {
		        $('.button-view-less').show();   
		    }
		    $('body').append('<h1 style="display: none" id="user_id">'+{{$user_id}}+'</h1>');
		    @if($isCompare)
			    displayOldSeoData();
			@endif
		    
		});

		/* internal link */
		var dem1 = document.getElementById("dem_noibo").value;
        document.getElementById("dem_noibo_show").innerHTML="Số liên kết nội bộ: <strong>"+dem1+"</strong><br/>";
		if (dem1 == "0"){
			var element = document.getElementById("view-less");
			element.parentNode.removeChild(element);
			document.getElementById("dem_noibo").value;
			document.getElementById("internal-link").innerHTML = '<div class="count-link">Không có liên kết nội bộ!</div>';
		}

		$(document).ready(function () {
		    var height = $(".view-link").find('div').height();
		    if(height <= $(".view-link").height())
		    {
		        $('.button-view-less').hide();
		    }
		    else 
		    {
		        $('.button-view-less').show();   
		    }
		});

		/* external link */
		var dem2 = document.getElementById("dem_ngoaibo").value;
        document.getElementById("dem_ngoaibo_show").innerHTML="Số liên kết ngoài: <strong>"+dem2+"</strong><br/>";
		if (dem2 == "0"){
			var element = document.getElementById("external");
			element.parentNode.removeChild(element);
			document.getElementById("external-link").innerHTML = '<div class="count-link">Không có liên kết ngoài!</div>';
		}

		$(document).ready(function () {
		    var height = $(".view-exter").find('div').height();
		    if(height <= $(".view-exter").height())
		    {
		        $('.button-view-less-exter').hide();
		    }
		    else 
		    {
		        $('.button-view-less-exter').show();   
		    }
		});

	</script>

@endsection