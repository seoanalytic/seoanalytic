@extends('users.layouts.master')

@section('content')

    <section id="history_form">
        <div class="intro">
            <h2 class="h2">Thông tin cá nhân</h2>
        </div>
        <div class="row">
            <div class="wrapper_profile">
                @if (!empty($user_info))
                <form action="{{route('users.updateProfile')}}" method=POST id="form_update_profile">
                {{ csrf_field() }}
                    <div class="form-group" id="profile_name">
                        <label class="label-icon medium"><span class="icon user font-fa" aria-hidden="true"></span>Họ và tên: </label>
                        <input name="profile_name" type="text" class="form-control input-md has-label-icon required" value="{{$user_info['name']}}" placeholder="Họ và tên">
                        <strong><span class="help-block"> </span></strong>
                    </div>
                    <div class="form-group">
                        <label class="label-icon medium"><span class="icon email font-fa" aria-hidden="true"></span>Địa chỉ Email: </label>
                        <input type="text" class="form-control input-md has-label-icon" value="{{$user_info['email']}}" readonly >
                    </div>
                    <div class="form-group">
                        <label class="label-icon medium"><i class="fa fa-calendar font-fa" aria-hidden="true"></i></span>Ngày khởi tạo: </label>
                        <input type="text" class="form-control input-md has-label-icon" value="{{$user_info['created_at']}}" readonly>
                    </div>
                    <button class="btn btn-green fl-right" type="submit" id="btn_update_profile"><i class="glyphicon glyphicon-ok"></i> Cập nhật</button>
                </form>
                @endif
            </div>
        </div>
    </section>

@endsection