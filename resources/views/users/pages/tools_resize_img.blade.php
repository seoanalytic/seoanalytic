@extends('users.layouts.master')

@section('content')
<!-- content -->
    
    <section id="history_form">
        <div class="intro">
            <h2 class="h2">Công cụ Resizer Image</h2>
        </div>
        <div class="panel panel-success">
            <div class="panel-heading">Chọn hình ảnh thay đổi kích thước</div>
                <div class="panel-body"> 
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                      <p class="error_item">{{ $error }}</p>
                    @endforeach
                    </div>
                @endif
                @if (Session::get('success'))
                    <div class="row">
                        <div class="col-md-12">
                        <div class="col-md-4">
                            <strong>Ảnh trước:</strong>
                        </div>
                        <div class="col-md-8">    
                            <img src="{{asset('normal_images/'.Session::get('imagename')) }}" />
                        </div>
                        </div>
                        <div class="col-md-12" style="margin-top:10px;">
                        <div class="col-md-4">
                            <strong>Ảnh sau:</strong>
                        </div>
                        <div class="col-md-8">    
                            <img src="{{asset('thumbnail_images/'.Session::get('imagename')) }}" /><br/>
                            <a download="img-resize"href="{{asset('thumbnail_images/'.Session::get('imagename')) }}" class="mar-t btn btn-green"><i class="icon-download-alt"> </i> Download</a>
                        </div>
                        </div>
                    </div>
                @endif
            {!! Form::open(array('route' => 'tool.postresizeimage','files'=>true)) !!}
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <br/>
                        {!! Form::file('photo', array('class' => 'form-control')) !!}
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <br/>
                        <button type="submit" class="btn btn-green">Upload Ảnh</button>
                    </div>
                </div>
                <div class="inputstyle">
                    <label>Chiều rộng(px)</label>
                    <input type="number" name="width" value="" placeholder="Width" min="10" max="2000" required="required">
                    <label>Chiều cao(px)</label>
                    <input type="number" name="height" value="" placeholder="Height" min="10" max="2000" required="required">
                </div>
            {!! Form::close() !!}
         </div>
        </div>
    </section>

<!-- /content -->
@endsection

@section('page_scripts')

@endsection