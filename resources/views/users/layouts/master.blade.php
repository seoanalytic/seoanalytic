<!DOCTYPE html>
<html lang="en">

<head>
    <link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
    <title>SEO Analytics</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noodp,index,follow"/>
    <meta name="_token" content="{{ csrf_token() }}"/>
    <!-- FONT CSS-->
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans" rel="stylesheet">
    <link href="{{ asset('assets/users/fonts/icons/style.min.css') }}" rel="stylesheet" type="text/css">
    <!-- LIBRARY CSS-->

    <!-- STYLE CSS-->
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&amp;subset=vietnamese" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/users/css/bootstrap.css') }}">
    <link href="{{ asset('assets/users/css/sweetalert.css') }}" rel="stylesheet" type="text/css" >
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/users/css/flaticon.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/users/css/magnific-popup.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/users/css/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/users/css/flaticon.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/users/css/lookup-history.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/users/css/animate.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/users/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/users/css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/users/css/loader_icon.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/users/css/responsive.css') }}">

</head>

<body>
    <!--top icon-->
    <div id="top"></div>
    <!--PAGE LOADER-->
    <div class="body-wrapper">
        <!-- HEDAER-->
        @include('users.templates.header')

        <!-- /HEDAER-->

        <!-- MAIN CONTENT-->
        <div class="main-content">
        @yield('content')
        </div>
        <!-- /MAIN CONTENT-->

        <!-- FOOTER-->
        @include('users.templates.footer')
        <!-- /FOOTER-->

        <!-- BUTTON BACK TO TOP-->
        <a href="#content" class="back-to-top"></a>
        
    </div>
    <!--/PAGE LOADER-->
    <!-- JS Facebook -->
    <!-- LIBRARY JS-->
    <script type="text/javascript" src="{{ asset('assets/users/js/jquery/jquery-3.2.1.min.js') }}"></script>
	
    <!-- <script type="text/javascript" src="{{ asset('assets/users/js/jquery-3.1.1.min.js') }}"></script> -->

    <script type="text/javascript" src="{{ asset('assets/users/js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('assets/admin/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/users/js/jquery.magnific-popup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/users/js/custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/users/js/master.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/users/js/seo-analyst.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>


    @yield('page_scripts')

</body>

</html>