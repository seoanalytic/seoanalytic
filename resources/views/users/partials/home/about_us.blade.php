<section id="ourteam" class="distance-bottom">
    <div class="container">
        <div class="intro">
            <h2 class="h2">Nhóm chúng tôi</h2>
            <h3 class="h3">Chúng tôi làm hết sức mình để đáp ứng nhu cầu khách hàng</h3>
        </div>
        <div class="wrapper_team">
            <div class="member1">
                    <div class="image col-md-6 col-lg-6 col-sm-6 col-xs-12">
                        <img src="{{ URL::asset('assets/users/img/hau.jpg') }}">
                    </div>
                    <div class="profile col-md-6 col-lg-6 col-sm-6 col-xs-12">
                        <strong>Trần Thị Thu Hậu</strong>
                        <span class="desrdivider">Nữ</span>
                        <p>Tôi luôn cố gắng làm ra trang web tốt nhất có thể để phục vụ khách hàng</p>

                        <div class="social">
                            <a class="ft_icon" href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a class="ft_icon" href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                            <a class="ft_icon" href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                            <a class="ft_icon" href=""><i class="fa fa-youtube" aria-hidden="true"></i></a>
                        </div>
                    </div>
            </div>
            <div class="member2">
                <div class="item">
                    <div class="image col-md-6 col-lg-6 col-sm-6 col-xs-12">
                        <img src="{{ URL::asset('assets/users/img/thao.jpg') }}">
                    </div>
                    <div class="profile col-md-6 col-lg-6 col-sm-6 col-xs-12">
                        <strong>Lê Thị Thu Thảo</strong>
                        <span class="desrdivider">Nữ</span>
                        <p>Tôi luôn cố gắng làm ra trang web tốt nhất có thể để phục vụ khách hàng</p>

                        <div class="social">
                            <a class="ft_icon" href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a class="ft_icon" href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                            <a class="ft_icon" href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                            <a class="ft_icon" href=""><i class="fa fa-youtube" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>