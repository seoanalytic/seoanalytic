<section id="contact" class="distance-bottom">
    <!-- start container -->
    <div class="container">
        <!-- start row -->
            <div class="intro col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <h2 class="h2">Liên hệ với chúng tôi</h2>
            </div>
            <!-- start showcase -->
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 col-xxs-12">
                <div class="map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15672.27000973168!2d106.7819284!3d10.8824705!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x9746dea7851b826a!2zS8OtIHTDumMgeMOhIGtodSBiIMSR4bqhaSBo4buNYyBxdeG7kWMgZ2lhIEhDTQ!5e0!3m2!1svi!2s!4v1494400416682" frameborder="0" allowfullscreen>
                    </iframe>
                </div>
            </div>
            <!-- end showcase -->

            <!-- start shorter column -->
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 col-xxs-12">
                <div class="form_wapper">
                    <form id="form_send_mail" action="{!! route('branch.send_faq') !!}" method="POST" enctype="multipart/form-data" accept-charset="UTF-8">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>
                        <div class="form-group clearfix {{$errors->has('fullname') ? 'has-error' : '' }}"> 
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-3">
                                <input type="text" name="fullname" class="effect-1" placeholder="Tên" value="{{old('fullname')}}">
                                <span class="focus-border"></span>
                                @if ($errors->has('fullname'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('fullname') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group clearfix {{$errors->has('email_questions') ? 'has-error' : '' }}">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="text" name="email_questions" class="" placeholder="E-mail" value="{{old('email_questions')}}">
                                @if ($errors->has('email_questions'))
                                <span class="help-block">
                                    <strong >{{ $errors->first('email_questions') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group clearfix {{$errors->has('content_questions') ? 'has-error' : '' }}"">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <textarea type="text" name="content_questions" class="" placeholder="Thông điệp">{{old('content_questions')}}</textarea>
                                @if ($errors->has('content_questions'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('content_questions') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group clearfix" id="btn_send_mail">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="btn_hover skip">
                                    <span class="hover-bg reset">
                                        <span class="hover-text reset">
                                            GỬI
                                        </span>
                                      </span>
                                    GỬI
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            <!-- end shorter column -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</section>