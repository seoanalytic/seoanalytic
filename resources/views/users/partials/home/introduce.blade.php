<section id="function-tool" class="distance-bottom">
    <div class="container">
        <div class="intro">
            <h2 class="h2">Giới Thiệu</h2>
        </div>
        <div class="col-md-3 col-xs-6 col-xxs-12 padd-bottom">
            <div class="function">
                <div class="icon">
                    <img src="{{ URL::asset('assets/users/img/icon-01.png') }}">
                </div>
                <div class="title">
                    <h2 class="h4">Phân tích</h2>
                </div>
                <div class="flipped">
                    <div class="title">
                        <h2 class="h4">Phân tích</h2>
                    </div>
                    <div class="content">
                        <p>Phân tích, đánh giá đưa ra lời khuyên hữu ích cho khách hàng để tối ưu hóa với các công cụ tìm kiếm</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-xs-6 col-xxs-12 padd-bottom">
            <div class="function">
                <div class="icon">
                    <img src="{{ URL::asset('assets/users/img/icon-02.png') }}">
                </div>
                <div class="title">
                    <h2 class="h4">Công cụ</h2>
                </div>
                <div class="flipped">
                    <div class="title">
                        <h2 class="h4">Công cụ</h2>
                    </div>
                    <div class="content">
                        <p>Cung cấp công cụ hỗ trợ cho khách hàng để tối ưu hóa với các công cụ tìm kiếm</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-xs-6 col-xxs-12">
            <div class="function">
                <div class="icon">
                    <img src="{{ URL::asset('assets/users/img/icon-03.png') }}">
                </div>
                <div class="title">
                    <h2 class="h4">Kiểm tra</h2>
                </div>
                <div class="flipped">
                    <div class="title">
                        <h2 class="h4">Kiểm tra</h2>
                    </div>
                    <div class="content">
                        <p>Kiểm tra sitemap, tốc độ tải trang,... cho khách hàng để tối ưu hóa với các công cụ tìm kiếm</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-xs-6 col-xxs-12">
            <div class="function">
                <div class="icon">
                    <img src="{{ URL::asset('assets/users/img/icon-04.png') }}">
                </div>
                <div class="title">
                    <h2 class="h4">Phản hồi</h2>
                </div>
                <div class="flipped">
                    <div class="title">
                        <h2 class="h4">Phản hồi</h2>
                    </div>
                    <div class="content">
                        <p>Giải đáp thắc mắc, chi tiết mọi câu hỏi của khách hàng</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>