<section id="Demo">
    <div class="container">
        <div class="intro">
            <h2 class="h2">Xem video Demo Sản Phẩm</h2>
        </div>
        <div class="intro-text col-md-7 col-lg-7 col-sm-12 col-xs-12">
            <p>SEO Analytics được thiết kế kết hợp các khả năng SEO to lớn với sự đơn giản và dễ sử dụng. Hệ thống này thực sự hướng dẫn và hỗ trợ bạn trong mọi công việc.</p>
            <p>Chức năng mà hệ thống cung cấp cho khách hàng:</p>
            <ul class="circle-gray">
                <li><strong>SEO Onpage</strong> Kiểm tra trang web</li>
                <li><strong>SEO Offpage</strong> Kiểm tra backlink</li>
                <li><strong>Công cụ hỗ trợ</strong> tối ưu</li>
                <li>Hướng dẫn khắc phục lỗi + Tối ưu hóa</li>
                <li>Báo cáo phân tích</li>
                <li>...</li>
                <li>...</li>
                <li>...</li>
            </ul>
            <a class="popup-youtube visible-sm visible-xs" href="https://www.youtube.com/watch?v=cYq2yG6mo4U&t=344s" data-popup="video">Xem video Demo </a>
        </div>
        <div class="video col-md-5 col-lg-5 col-sm-12 col-xs-12">
            <a class="popup-youtube" href="https://www.youtube.com/watch?v=HWijLsd4qlg" data-popup="video">
            <img src="{{ URL::asset('assets/users/img/video-poster.jpg') }}" class="img-circle">
            <i class="fa fa-play"></i>
            </a>
        </div>
    </div>
</section>