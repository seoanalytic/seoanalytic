<section id="WIS">
    <div class="container">
        <div class="intro">
            <h2 class="h2">Lợi ích từ SEO</h2>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 padd-bottom border-WIS">
            <div class="outer_icon">
                <div class="icon_wis icon_chart">

                </div>
            </div>
            <div class="description_icon">
                Tăng lưu lượng site
            </div>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 padd-bottom border-WIS">
            <div class="outer_icon">
                <div class="icon_wis icon_money">

                </div>
            </div>
            <div class="description_icon">
                Tăng lợi nhuận

            </div>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 border-WIS">
            <div class="outer_icon">
                <div class="icon_wis icon_request">

                </div>
            </div>
            <div class="description_icon">
                Tăng số lượng request và cuộc gọi đến trang web
            </div>
        </div>
        <div class="col-md-3 col-lg-3 col-sm-6 col-xs-6 border-WIS">
            <div class="outer_icon">
                <div class="icon_wis icon_awareness">

                </div>
            </div>
            <div class="description_icon">
                Tăng số lượng nhận biết nhãn hiệu
            </div>
        </div>
    </div>
</section>