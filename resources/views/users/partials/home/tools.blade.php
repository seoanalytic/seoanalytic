<section>
    <div class="container">
        <div class="intro">
            <h2 class="h2">Các công cụ hỗ trợ SEO</h2>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-6 tool-wrapper">
            <a href="{{ route('tool.getresizeimage') }}">
                <i class="fa fa-arrows-alt" aria-hidden="true"></i>
                <span class="tool-footer">
                    Thay đổi kích thước ảnh
                </span>
            </a>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-6 tool-wrapper">
            <a href="{{ route('tool.word') }}">
                <i class="fa fa-file-word-o" aria-hidden="true"></i>
                <span class="tool-footer">
                    Bộ đếm từ
                </span>
            </a>
        </div>
        <!-- <div class="col-md-3 col-sm-6 col-xs-6 tool-wrapper">
            <a href="">
                <i class="glyphicon glyphicon-duplicate"></i>
                <span class="tool-footer">
                    Kiểm tra đạo văn
                </span>
            </a>
        </div> -->
        <!-- <div class="col-md-3 col-sm-6 col-xs-6 tool-wrapper">
            <a href="{{ route('tool.sitemap') }}">
                <i class="fa fa-sitemap" aria-hidden="true"></i>
                <span class="tool-footer">
                    Tạo XML Sitemap
                </span>
            </a>
        </div> -->
        <div class="col-md-4 col-sm-6 col-xs-6 tool-wrapper">
            <a href="{{ route('tool.favicon') }}">
                <i class="fa fa-picture-o" aria-hidden="true"></i>
                <span class="tool-footer">
                    Tạo Favicon
                </span>
            </a>
        </div>
    </div>
</section>