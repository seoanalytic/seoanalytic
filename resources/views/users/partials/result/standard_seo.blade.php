<section id="standard_SEO">
	<div class="container ">
		<div class="row ">
			<div class="col-md-12 title_css padded_result">Tiêu chuẩn SEO</div>
		</div>
		
		<div class="row css_border">
			<div class="clearfix"></div>
			<div class="col-md-3 css_text factor standard_seo padded_result ">
				Đường dẫn URL
				<i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="Đường dẫn của website cho các công cụ tìm kiếm dễ dàng đọc, lập chỉ mục, dễ nhớ và ngắn gọn."></i>
			</div>
			@foreach ($url as $item)
			<div class="col-md-9 bd padded_result">
				<div class="report-icon">
					<?php 
						$parts = parse_url($domain);
						$scheme = $parts[ "scheme" ];
						if ($scheme == "http")
							$j = substr($item,7);
						else $j = substr($item,8);
						$i = mb_strlen($j, 'UTF-8');
						if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬]/', $item) || $i < 10 || $i > 96)
						{
    						echo '<i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>';
						}
						else echo '<i class="fa fa-check-circle-o" aria-hidden="true" data-toggle="popover" data-content="OK"></i>';
					?>
				</div>
				<div class="analytic-detail standard_seo">
					<?php 
						$parts = parse_url($domain);
						$scheme = $parts[ "scheme" ];
						if ($scheme == "http")
							$j = substr($item,7);
						else $j = substr($item,8);
						$i = mb_strlen($j, 'UTF-8');
						echo '<a href="'.$item.'" target="_blank">'.$j.'</a>';
						echo '<br/>&rarr; '.$i.' ký tự. ';
						$button='<button type="button" class="mar-t btn-sweet" data-toggle="modal" data-target="#url">Khắc phục</button>';
						//&& ( $i < 10 || $i > 96)
						if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬]/', $item))
						{
    						echo '</br>';
    						echo '&rarr; Đường dẫn không thân thiện.';
						}
						else 
						{	
							echo '</br>';
    						echo '&rarr; Đường dẫn thân thiện.';
						} 
						if (!preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬]/', $item) && ( $i > 10 && $i < 96))
						{
    						$button='';
						}
						echo '</br>';
						echo $button;
					?>
					<div class="modal fade" id="url" role="dialog">
                        <div class="modal-vertical-alignment-helper">
                            <div class="modal-dialog modal-vertical-align-center">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title"><b>Hướng dẫn khắc phục đường dẫn - URL</b></h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Để giải quyết vấn đề này, bạn nên:</p>
                                        <ul>
                                            <li>Dùng gạch nối (-) để phân cách các từ với nhau.</li>
                                            <li>Đường dẫn phải có chứa từ khóa.</li>
                                            <li>Đường dẫn không chứa các ký tự như ?, #, =, @, %, $, gây khó khăn cho công cụ tìm kiếm và làm giảm lập chỉ mục index của website.</li>
                                            <li>Chiều dài tối ưu cho đường dẫn nằm trong khoảng từ 10 đến 96 ký tự.</li>
                                        </ul>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default button-close" data-dismiss="modal">Đóng</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
			@endforeach
		</div>

		<div class="row css_border">
			<div class="clearfix"></div>
			<div class="col-md-3 css_text factor standard_seo padded_result ">
				Thẻ canonical 
				<i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="Thuộc tính nằm trong mã HTML, nó cho phép quản trị website ngăn được trùng lặp nội dung thông qua việc xác định thẻ canonical"></i>
			</div>
			<div class="col-md-9 bd padded_result">
				@foreach($link as $can)
				<div class="report-icon" id="icon-can">
					
				</div>
				<div class="analytic-detail standard_seo" id="canonical">
					<?php 
						$can ->each(function($node){
							if (strtolower($node->attr('rel')) == 'canonical'){
	    						echo '<p id="href-can" >'.$node->attr('href'). '</p>';

	    					}
	    				});
					?>
				</div>
				@endforeach
			</div>
		</div>

		<div class="row css_border">
			<div class="clearfix"></div>
			<div class="col-md-3 css_text factor standard_seo padded_result ">
				Title
				<i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="Thẻ này hiển thị tiêu đề trang trong kết quả công cụ tìm kiếm, ở đầu trình duyệt của người dùng. Tiêu đề là yếu tố quan trọng nhất trên trang bắt buộc chứa từ khóa."></i>
			</div>
			@foreach ($title as $item) 
				<div class="col-md-9 bd padded_result">
					<div class="report-icon">
						<?php 
							$i = mb_strlen($item, 'UTF-8');
							if ($i < 10 || $i > 70) {
								echo '<i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>';
							}
							else echo '<i class="fa fa-check-circle-o" aria-hidden="true" data-toggle="popover" data-content="OK"></i>';
						?>
					</div>
					<div class="analytic-detail standard_seo">
						<?php
							echo $item;
							echo "</br>";
							$i = mb_strlen($item, 'UTF-8');
							echo '&rarr; '.$i.' ký tự. ';
							if ($i < 10 || $i > 70){
								echo '<button type="button" class="mar-t btn-sweet" data-toggle="modal" data-target="#title">Khắc phục</button>';
							}
						?>
						<div class="modal fade" id="title" role="dialog">
	                        <div class="modal-vertical-alignment-helper">
	                            <div class="modal-dialog modal-vertical-align-center">
	                                <!-- Modal content-->
	                                <div class="modal-content">
	                                    <div class="modal-header">
	                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
	                                        <h4 class="modal-title"><b>Hướng dẫn khắc phục Tiêu đề - Title</b></h4>
	                                    </div>
	                                    <div class="modal-body">
	                                        <p>Thẻ tiêu đề là yếu tố quan trọng nhất của trang để giải quyết vấn đề này, bạn nên:</p>
	                                        <ul>
	                                            <li>Tạo tiêu đề duy nhất cho mỗi trang của website.</li>
	                                            <li>Tiêu đề phải có chứa từ khóa.</li>
	                                            <li>Chiều dài tối ưu cho tiêu đề nằm trong khoảng từ 10 đến 70 ký tự.</li>
	                                        </ul>
	                                    </div>
	                                    <div class="modal-footer">
	                                        <button type="button" class="btn btn-default button-close" data-dismiss="modal">Đóng</button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
					</div>
				</div>
			@endforeach
		</div>
		
		<div class="row css_border">
			<div class="clearfix"></div>
			<div class="col-md-3 css_text factor standard_seo padded_result ">
				Meta keywords
				<i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="Từ khóa chính website. Quy tắc không được chấp nhận và sẽ sớm được gỡ bỏ. Xin vui lòng, không dựa vào nó."></i>
			</div>
			
			<div class="col-md-9 bd padded_result">

				<div class="report-icon">
					<!-- <i class="fa fa-info-circle" aria-hidden="true" data-toggle="popover" data-content="WARNING"></i> -->
				</div>
				@foreach ($meta as $item)
				<div class="analytic-detail standard_seo" id="meta-keywords">
					<?php 
						echo '';
						$item ->each(function($node){
							if (strtolower($node->attr('name')) == 'keywords'){
	    						echo $node->attr('content');
	    					}
	    				});
					?>
				</div>
				@endforeach
			</div>
			
		</div>
	
		<div class="row css_border">
			<div class="clearfix"></div>
			<div class="col-md-3 css_text factor standard_seo padded_result ">
				Meta description
				<i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="Giới thiệu ngắn gọn nội dung trang web. Nội dung mô tả được Google sử dụng"></i>
			</div>
			@foreach ($meta as $item)
			<div class="col-md-9 bd padded_result">
				<div class="report-icon" id="icon-description">
					<?php 
						$item ->each(function($node){
							if (strtolower($node->attr('name')) == 'description'){
	    						$i = mb_strlen($node->attr('content'), 'UTF-8');
								if ($i < 70 || $i > 160){
									echo '<i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>';
							}
							else echo '<i class="fa fa-check-circle-o" aria-hidden="true" data-toggle="popover" data-content="OK"></i>';
	    					}
	    				});
					?>
				</div>
				<div class="analytic-detail standard_seo" id="meta-description">
					<?php 
						$item ->each(function($node){
							if (strtolower($node->attr('name')) == 'description'){
	    						echo $node->attr('content');
	    						echo "</br>";
	    						$i = mb_strlen($node->attr('content'), 'UTF-8');
	    						echo '&rarr; '.$i.' ký tự. ';
								if ($i < 70 || $i > 160){
									echo '<button type="button" class="mar-t btn-sweet" data-toggle="modal" data-target="#description">Khắc phục</button>';
								}
	    					}
	    				});
					?>
					<div class="modal fade" id="description" role="dialog">
                        <div class="modal-vertical-alignment-helper">
                            <div class="modal-dialog modal-vertical-align-center">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title"><b>Hướng dẫn khắc phục Meta description</b></h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Meta description đóng vai trò như một lời giới thiệu với mọi người về trang web để giải quyết vấn đề này, bạn nên:</p>
                                        <ul>
                                            <li>Meta description phải có chứa từ khóa.</li>
                                            <li>Chiều dài tối ưu cho meta description nằm trong khoảng từ 70 đến 160 ký tự (khoảng 25 - 30 từ).</li>
                                        </ul>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default button-close" data-dismiss="modal">Đóng</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
			@endforeach
		</div>

		<div class="row css_border">
			<div class="clearfix"></div>
			<div class="col-md-3 css_text factor standard_seo padded_result ">
				Headings
				<i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="Cần có đủ 6 thẻ H1, H2, H3, H4, H5, H6. Độ quan trọng giảm dần từ H1 đến H6. Trong đó H1, H2 nên chứa từ khóa chính và H3 chứa từ khóa phụ. Các thẻ này đặt trên một dòng, không đặt lên cả đoạn"></i>
			</div>
			
			<div class="col-md-9 bd padded_result">
				<div class="report-icon">
					<?php
						foreach ($h as $i => $item){
							$i=substr($i,-1);
							if (intval($i)==1) {
								if (count($item) > 1 || count($item) == 0)
									echo '<i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>';
								else
									echo '<i class="fa fa-check-circle-o" aria-hidden="true" data-toggle="popover" data-content="OK"></i>';
							}
						}
					?>
				</div>
				<div class="analytic-detail standard_seo view-h">
					@foreach ($h as $i => $item)
						<?php
							$i=substr($i,-1);
							echo '<b>Heading '.$i.': </b>';
							if (intval($i)==1)
							{
								if (count($item) > 1)
									echo '<mark>Trang của bạn sử dụng khá nhiều thẻ <code>H'.$i.'</code>. <i>Khuyến cáo duy nhất 1 thẻ H'.$i.'</i></pre></mark>';
								if (count($item) == 0)
									echo '<mark>Trang của bạn không có thẻ <code>H'.$i.'</code>. <i>Khuyến cáo duy nhất 1 thẻ H'.$i.'</i></pre></mark>';
							}
							// if (intval($i)==2 || intval($i)==3 || intval($i)==4 || intval($i)==5 || intval($i)==6)
							// {
							// 	if (count($item) > 10)
							// 		echo 'Trang của bạn sử dụng khá nhiều thẻ <code>H'.$i.'</code>. <i>Khuyến cáo ít hơn 10 thẻ H'.$i.'</i></pre>';
							// }
							echo "</br>";
							if(count($item)==0)
							{
								echo 'Không có!';
								echo '</br>';
							}

							$item ->each(function($node) use($i){
									if(!empty(trim($node->text())))
									{
										echo 'h'.$i.': '.$node->text();
									}
									else
										echo 'h'.$i.': '.$node->html();
									echo "</br>";
							});
						?>
					@endforeach
				</div>
				<div class="button-view-less">
                    <p>
                        <a href="javascript:void(0)" class="btn-view-less">Xem thêm</a>
                    </p>
                </div>
			</div>
		</div>

		<div class="row css_border">
			<div class="clearfix"></div>
			<div class="col-md-3 css_text factor standard_seo padded_result ">
				Thuộc tính ALT của Images
				<i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="Là một văn bản mô tả nội dung cho hình ảnh đăng tải. Nếu hình ảnh không hiển thị được thì người dùng vẫn có thể biết được nội dung mà hình ảnh đó nói đến. Thẻ này rất quan trọng, nó giúp bọ Google hiểu được bức ảnh nói về nội dung gì, từ đó trả kết quả tìm kiếm chính xác cho người dùng"></i>
			</div>
			@foreach ($img as $item)
				<div class="col-md-9 bd padded_result">
					<div class="report-icon">
						<?php 
							$noALT=0;
							$totalImageTag = count($item);
							$item ->each(function($node) use(&$noALT){
								if (empty($node->attr('alt')))
									$noALT++;
							});
							if ($totalImageTag != 0){
								if($noALT !=0)
									echo '<i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>';
								else
									echo '<i class="fa fa-check-circle-o" aria-hidden="true" data-toggle="popover" data-content="OK"></i>';
							}
							else
							{
								echo '<i class="fa fa-check-circle-o" aria-hidden="true" data-toggle="popover" data-content="OK"></i>';
							}
						?>
					</div>
					<div class="analytic-detail standard_seo">
						<?php
							$noALT=0;
							$totalImageTag = count($item);
							$item ->each(function($node) use(&$noALT){
								if (empty($node->attr('alt')))
									$noALT++;
							});
							if ($totalImageTag != 0){
								if($noALT !=0){
									echo 'Trang web có tổng cộng <strong>'.$totalImageTag.'</strong> thẻ img nhưng có <strong>'.$noALT.'</strong> thẻ bị thiếu thuộc tính "alt".';
									echo '<button type="button" class="mar-t btn-sweet" data-toggle="modal" data-target="#img">Khắc phục</button>';
								}
								else
									echo 'Chúc mừng, tất cả các thẻ img của trang web đều có trường "alt".';
							}
							else
							{
								echo 'Trang web không có thẻ <b>img</b>.';
							}
						?>
						<div class="modal fade" id="img" role="dialog">
	                        <div class="modal-vertical-alignment-helper">
	                            <div class="modal-dialog modal-vertical-align-center">
	                                <!-- Modal content-->
	                                <div class="modal-content">
	                                    <div class="modal-header">
	                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
	                                        <h4 class="modal-title"><b>Hướng dẫn khắc phục Image</b></h4>
	                                    </div>
	                                    <div class="modal-body">
	                                        <p>Search engine không đọc được nội dung bên trong file ảnh (ảnh xấu hay đẹp thì chỉ người dùng mới biết). Nó chỉ đọc được những nội dung văn bản sau đây:</p>
	                                        <ul>
	                                            <li>Tên file ảnh: chứa từ khóa, viết liền, không dấu, phân tách các từ bởi dấu trừ (-).</li>
	                                            <li>Thẻ ALT (văn bản thay thế): một dòng mô tả nội dung ảnh, chứa từ khóa. Nội dung trong thẻ ALT được ẩn phía sau ảnh.</li>
	                                            <li>Caption (chú thích): một dòng mô tả xuất hiện ngay phía dưới ảnh, chứa từ khóa.</li>
	                                        </ul>
	                                    </div>
	                                    <div class="modal-footer">
	                                        <button type="button" class="btn btn-default button-close" data-dismiss="modal">Đóng</button>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
					</div>
				</div>
			@endforeach
		</div>
	
		<div class="row css_border">
			<div class="clearfix"></div>
			<div class="col-md-3 css_text factor standard_seo padded_result ">
				Favicon 
				<i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="Được xem như là một logo thu nhỏ, giúp người dùng dễ dàng nhận ra trang"></i>
			</div>
			<div class="col-md-9 bd padded_result">
				@foreach($link as $icon)
				<div class="report-icon" id="icon-favicon">
					
				</div>
				<div class="analytic-detail standard_seo" id="content-favicon">
					<?php 
						$icon ->each(function($node){
							if (strtolower($node->attr('rel')) == 'shortcut icon' || strtolower($node->attr('rel')) == 'icon'){
	    						echo '<p id="img-favicon" >'.$node->attr('href'). '</p>';
	    					}
	    				});
					?>
				</div>
				@endforeach
			</div>
		</div>

		<div class="row css_border">
			<div class="clearfix"></div>
			<div class="col-md-3 css_text factor standard_seo padded_result ">
				XML Sitemaps 
				<i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="XML Sitemaps là danh sách địa chỉ các trang web trên một website. Đây là cách để robot tìm kiếm và truy cập nhanh các trang web."></i>
			</div> 
			<div class="col-md-9 bd padded_result">
					<?php 
						$checkForFiles = array('sitemap.xml');
						foreach($checkForFiles as $file){
							$parts = parse_url($domain);
						    $url = $parts[ "scheme" ] . '://' .$parts['host'].'/'. $file;
						    $ch = curl_init ($url);
						    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
						    curl_setopt($ch, CURLOPT_HEADER, 1);

						    $output = curl_exec ($ch);

						    if(curl_getinfo($ch)['http_code'] != 200){
						    	echo '<div class="report-icon">
										<i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>
									</div> ';
								echo '<div class="analytic-detail standard_seo">';
						        echo 'Trang web không có ' . $file.'<br/>';
						        echo '<button type="button" class="mar-t btn-sweet" data-toggle="modal" data-target="#test-sitemap">Khắc phục</button>';
						        echo '</div>';

						    }else{
						    	echo '<div class="report-icon">
						    			<i class="fa fa-check-circle-o" aria-hidden="true" data-toggle="popover" data-content="OK"></i>
						    		</div>';
						    	echo '<div class="analytic-detail standard_seo">';
						        echo 'Chúc mừng! Trang web có '.$file . ': <a target="_blank" href="'.$parts[ "scheme" ] . '://' . $parts[ "host" ] .'/'. $file.'">' . $parts[ "scheme" ] . '://' . $parts[ "host" ] .'/'. $file .'</a>' . '<br>';
						        echo '</div>';
						    }
						    curl_close($ch);
						}
					?>
				<div class="modal fade" id="test-sitemap" role="dialog">
                    <div class="modal-vertical-alignment-helper">
                        <div class="modal-dialog modal-vertical-align-center">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><b>Hướng dẫn khắc phục XML Sitemaps</b></h4>
                                </div>
                                <div class="modal-body">
                                    <a href="https://www.xml-sitemaps.com/" style="font-size: 16px;">Tạo XML Sitemap</a>
                                    <p style="font-size: 16px;">Cấu trúc file sitemap.xml</p>
                                    <div class="style-sitemap">
                                    	<img src="{{ asset('assets/users/img/sitemap.png') }}">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default button-close" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>

		<div class="row css_border">
			<div class="clearfix"></div>
			<div class="col-md-3 css_text factor standard_seo padded_result ">
				Robots.txt 
				<i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="Robots.txt chứa các dòng lệnh dùng để CẤM robots không được truy cập đến một số tài nguyên trên website."></i>
			</div>
			<div class="col-md-9 bd padded_result">
					<?php 
						$checkForFiles = array('robots.txt');
						foreach($checkForFiles as $file){
							$parts = parse_url($domain);
						    $url = $parts[ "scheme" ] . '://' .$parts['host'].'/'. $file;
						    $ch = curl_init ($url);
						    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
						    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
						    curl_setopt($ch, CURLOPT_HEADER, 1);

						    $output = curl_exec ($ch);

						    if(curl_getinfo($ch)['http_code'] != 200){
						    	echo '<div class="report-icon">
										<i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>
									</div> ';
								echo '<div class="analytic-detail standard_seo">';
								echo 'Trang web không có ' . $file.'<br/>';
						        echo '<button type="button" class="mar-t btn-sweet" data-toggle="modal" data-target="#test-robot">Khắc phục</button>';
						        echo '</div>';

						    }else{
						    	echo '<div class="report-icon">
						    			<i class="fa fa-check-circle-o" aria-hidden="true" data-toggle="popover" data-content="OK"></i>
						    		</div>';
						    	echo '<div class="analytic-detail standard_seo">';
						        echo 'Chúc mừng! Trang web có '.$file . ': <a target="_blank" href="'.$parts[ "scheme" ] . '://' . $parts[ "host" ] .'/'. $file.'">' . $parts[ "scheme" ] . '://' . $parts[ "host" ] .'/'. $file .'</a>' . '<br>';
						        echo '</div>';
						    }
						    curl_close($ch);
						}
					?>
				<div class="modal fade" id="test-robot" role="dialog">
                    <div class="modal-vertical-alignment-helper">
                        <div class="modal-dialog modal-vertical-align-center">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><b>Hướng dẫn khắc phục Robot.txt</b></h4>
                                </div>
                                <div class="modal-body">
                                    <a href="https://smallseotools.com/robots-txt-generator/" style="font-size: 16px;">Tạo Robot.txt</a>
                                    <p style="font-size: 16px;">Cấu trúc file robot.txt</p>
                                    <div class="style-robot">
                                    	<p>User-agent: đối tượng bot được chấp nhận</p>
                                    	<p>Disallow/Allow: URL muốn chặn/cho phép</p>
                                    	<p>Nếu dùng *: Đại diện cho tất cả</p>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default button-close" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>

	</div>
</section>