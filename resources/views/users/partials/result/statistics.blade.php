<section id="Overview" class="container">
	<div id="results-header" class="intro">
		<h2 class="h2">Phân tích - Đánh giá SEO</h2>
		<div class="url-name">
			@foreach ($url as $item) 
				<a href="{{$item}}" target="_blank"><?php echo $item; ?></a>
			@endforeach
		</div>
	</div>
		<div id="download-report">
			<a id="html2pdf" class="btn"> <span class="glyphicon glyphicon-download-alt"> PDF</span></a>
		</div>
</section>
<div class="clearfix"></div>