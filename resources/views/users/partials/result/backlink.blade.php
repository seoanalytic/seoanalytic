<section id="backlink">
	<div class="container">
        <div class="row ">
            <div class="col-md-12 title_css padded_result">Seo Offpage</div>
        </div>

        <div class="row css_border">
            <div class="clearfix"></div>
            <div class="col-md-3 css_text factor padded_result backlink_seo">
                Thứ hạng tại Alexa quốc tế
                <i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="Alexa Rank là thước đo mức độ phổ biến của các website. Chỉ số thứ hạng của mỗi website được Alexa kết hợp đánh giá từ 2 yếu tố là số trang web người dùng xem (Page Views) và số người truy cập website (Reach)."></i>
            </div>
            <div class="col-md-9 bd padded_result">
                <div class="report-icon">
                    <i class="fa fa-info-circle" aria-hidden="true" data-toggle="popover" data-content=""></i>
                </div>
                @foreach ($url as $item)
                <div class="analytic-detail backlink_seo">
                    <?php
                        $xml = simplexml_load_file('http://data.alexa.com/data?cli=10&dat=snbamz&url='.$item);
                        $rank=isset($xml->SD[1]->POPULARITY)?$xml->SD[1]->POPULARITY->attributes()->TEXT:0;
                        $delta=isset($xml->SD[1]->RANK)?$xml->SD[1]->RANK ->attributes()->DELTA:0;
                        $web=(string)$xml->SD[0]->attributes()->HOST;
                        echo "<strong>".$web."</strong> có thứ hạng Alexa trên trường quốc tế là <strong> ".$rank."</strong>";
                        if (substr($delta, 0, 1) == "-")
                            echo " &rarr; Tăng <strong>".substr($delta, 1)."</strong> lần so với 3 tháng trước.";
                        else echo " &rarr; Giảm <strong>".substr($delta, 1)."</strong> lần so với 3 tháng trước.";
                        echo '<button type="button" class="mar-t btn-sweet btn-tip" data-toggle="modal" data-target="#alexa">Hướng dẫn</button>';
                    ?>
                </div>
                @endforeach
                <div class="modal fade" id="alexa" role="dialog">
                    <div class="modal-vertical-alignment-helper">
                        <div class="modal-dialog modal-vertical-align-center">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><b>Hướng dẫn tăng thứ hạng Alexa</b></h4>
                                </div>
                                <div class="modal-body">
                                    <ul style="font-size: 16px;">
                                        <li>Tăng lưu lượng truy cập vào website của bạn.</li>
                                        <li>Cài đặt thanh công cụ Alexa.</li>
                                        <li>Chèn Alexa rank widget vào website.</li>
                                        <li>Khuyến khích khách truy cập sử dụng thanh công cụ Alexa.</li>
                                        <li>Xác minh trang web của bạn trên Alexa.com.</li>
                                        <li>Tạo backlinks chất lượng cho website của bạn.</li>
                                        <li>Viết nội dung hữu ích và chất lượng.</li>
                                        <li>Kết nối với các mạng xã hội.</li>
                                        <li>Gửi website của bạn đến các thư viện web.</li>
                                    </ul>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default button-close" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row css_border">
            <div class="clearfix"></div>
            <div class="col-md-3 css_text factor padded_result backlink_seo">
                Thứ hạng trong nước
                <i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="Alexa Rank là thước đo mức độ phổ biến của các website. Chỉ số thứ hạng của mỗi website được Alexa kết hợp đánh giá từ 2 yếu tố là số trang web người dùng xem (Page Views) và số người truy cập website (Reach)."></i>
            </div>
            <div class="col-md-9 bd padded_result">
                <div class="report-icon">
                    <i class="fa fa-info-circle" aria-hidden="true" data-toggle="popover" data-content=""></i>
                </div>
                @foreach ($url as $item)
                <div class="analytic-detail backlink_seo">
                    <?php
                        $xml = simplexml_load_file('http://data.alexa.com/data?cli=10&dat=snbamz&url='.$item);
                        $web=(string)$xml->SD[0]->attributes()->HOST;
                        if (isset($xml->SD[1]->COUNTRY) && ($xml->SD[1]->COUNTRY->attributes()->CODE == "VN")) {
                            echo "<strong>".$web."</strong> có thứ hạng trong nước là<strong> ".$xml->SD[1]->COUNTRY->attributes()->RANK."</strong>.";
                            echo '<button type="button" class="mar-t btn-sweet btn-tip" data-toggle="modal" data-target="#alexa">Hướng dẫn</button>';
                        }
                        else echo "Chưa cập nhật!";
                    ?>
                </div>
                @endforeach
                <div class="modal fade" id="alexa" role="dialog">
                    <div class="modal-vertical-alignment-helper">
                        <div class="modal-dialog modal-vertical-align-center">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><b>Hướng dẫn tăng thứ hạng Alexa</b></h4>
                                </div>
                                <div class="modal-body">
                                    <ul style="font-size: 16px;">
                                        <li>Tăng lưu lượng truy cập vào website của bạn.</li>
                                        <li>Cài đặt thanh công cụ Alexa.</li>
                                        <li>Chèn Alexa rank widget vào website.</li>
                                        <li>Khuyến khích khách truy cập sử dụng thanh công cụ Alexa.</li>
                                        <li>Xác minh trang web của bạn trên Alexa.com.</li>
                                        <li>Tạo backlinks chất lượng cho website của bạn.</li>
                                        <li>Viết nội dung hữu ích và chất lượng.</li>
                                        <li>Kết nối với các mạng xã hội.</li>
                                        <li>Gửi website của bạn đến các thư viện web.</li>
                                    </ul>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default button-close" data-dismiss="modal">Đóng</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="row css_border">
            <div class="clearfix"></div>
            <div class="col-md-3 css_text factor padded_result backlink_seo">
                Liên kết nội bộ
                <i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="là những liên kết qua lại giữa các trang trong cùng một tên miền"></i>
            </div>
            <div class="col-md-9 bd padded_result">
                <div class="report-icon">
                    <i class="fa fa-info-circle" aria-hidden="true" data-toggle="popover" data-content=""></i>
                </div>
                <?php $dem = 0;?>
                @foreach ($url as $item)
                <div class="analytic-detail view-link backlink_seo" id="internal-link">
                    <span class="count-link" id="dem_noibo_show"></span>
                    <?php
                        $opts = [
                            'http' => [
                                'method' => 'GET',
                                'header' => [
                                        'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'
                                ]
                            ]
                        ];

                        $html = file_get_contents($item, false, stream_context_create($opts));

                        $doc = new DOMDocument();
                        libxml_use_internal_errors(true);
                        $doc->loadHTML($html); //helps if html is well formed and has proper use of html entities!
                        libxml_clear_errors();
                        $xpath = new DOMXpath($doc);

                        $hrefs = $xpath->evaluate("/html/body//a");
                        $parts = parse_url($domain);
                        // if($item[strlen($item)-1]=='/')
                        //     $item=substr($item,0,strlen($item)-1);
                        $item = '://' .$parts['host'];
                        for($i = 0; $i < $hrefs->length; $i++){
                            $href = $hrefs->item($i);
                            $link = $href->getAttribute('href');
                            $link = filter_var($link, FILTER_SANITIZE_URL);
                            // validate url
                            if((!filter_var($link, FILTER_VALIDATE_URL)===false) && (strpos($link, $item)>0)){
                                echo '<a target="_blank" href="'.$link.'">'.$link.'</a><br /><hr />';
                                $dem++;
                            }
                        }
                        echo '<input type="hidden" value="'.$dem.'" id="dem_noibo">';
                    ?>
                </div>
                @endforeach
                <div class="button-view-less-link" id=view-less>
                    <p>
                        <a href="javascript:void(0)" class="btn-view-less-link">Xem thêm</a>
                    </p>
                </div>
            </div>
        </div>

        <div class="row css_border">
            <div class="clearfix"></div>
            <div class="col-md-3 css_text factor padded_result backlink_seo">
                Liên kết ngoài
                <i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content=" là những liên kết trỏ từ trang web này với trang web khác chứa các liên kết này. Nói 1 cách dễ hiểu, nếu 1 website khác link đến web của bạn thì có thể coi đó là liên kết ngoài."></i>
            </div>
            <div class="col-md-9 bd padded_result">
                <div class="report-icon">
                    <i class="fa fa-info-circle" aria-hidden="true" data-toggle="popover" data-content=""></i>
                </div>
                <?php $dem = 0;?>
                @foreach ($url as $item)
                <div class="analytic-detail view-exter backlink_seo" id="external-link">
                    <span class="count-link" id="dem_ngoaibo_show"></span>
                    <?php
                        $opts = [
                            'http' => [
                                'method' => 'GET',
                                'header' => [
                                        'User-Agent: MMozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'
                                ]
                            ]
                        ];

                        $html = file_get_contents($item, false, stream_context_create($opts));

                        $doc = new DOMDocument();
                        libxml_use_internal_errors(true);
                        $doc->loadHTML($html); //helps if html is well formed and has proper use of html entities!
                        libxml_clear_errors();
                        $xpath = new DOMXpath($doc);

                        $hrefs = $xpath->evaluate("/html/body//a");
                        $parts = parse_url($domain);
                        // if($item[strlen($item)-1]=='/')
                        //     $item=substr($item,0,strlen($item)-1);
                        $item = '://' .$parts['host'];
                        
                        
                        for($i = 0; $i < $hrefs->length; $i++){
                            $href = $hrefs->item($i);
                            $link = $href->getAttribute('href');
                            $link = filter_var($link, FILTER_SANITIZE_URL);
                            // validate url
                            if((!filter_var($link, FILTER_VALIDATE_URL)===false) && (strpos($link, $item)==NULL) && (strpos($link, 'mailto')!==0)){
                                echo '<a target="_blank" href="'.$link.'">'.$link.'</a><br /><hr />';
                                $dem++;
                            }
                        }
                        echo '<input type="hidden" value="'.$dem.'" id="dem_ngoaibo">';
                    ?>
                    
                </div>
                @endforeach
                <div class="button-view-less-exter" id=external>
                    <p>
                        <a href="javascript:void(0)" class="btn-view-less-exter">Xem thêm</a>
                    </p>
                </div>
            </div>
        </div>

    </div>
</section>