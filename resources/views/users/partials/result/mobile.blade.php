<section id="mobile">
    <div class="container">
        <div class="row ">
            <div class="col-md-12 title_css padded_result">Mobile</div>
        </div>
        <div class="row css_border ">
            <div class="clearfix"></div>
            <div class="col-md-3 css_text factor mobile_seo padded_result ">
                Cấu hình vùng nhìn
                <i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="webpage cần gắn thẻ meta viewport để gán độ rộng và tỉ lệ co giãn thích hợp cho từng loại thiết bị"></i>
            </div>
            <div class="col-md-9 bd padded_result">
                <div class="report-icon">
                    <i class="fa fa-info-circle" aria-hidden="true" data-toggle="popover" data-content=""></i>
                </div>
                <div id="metaViewportTag" class="analytic-detail mobile_seo">
                    @include('users.partials.result.loader_icon')
                </div>
            </div>
        </div>

        <div class="row css_border ">
            <div class="clearfix"></div>
            <div class="col-md-3 css_text factor mobile_seo padded_result ">
                Kích cỡ font
                <i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="cỡ chữ, font chữ cần đủ lớn và dễ nhìn trên màn hình điện thoại để người dùng không cần phóng to để đọc nội dung. "></i>
            </div>
            <div class="col-md-9 bd padded_result">
                <div class="report-icon">
                    <i class="fa fa-info-circle" aria-hidden="true" data-toggle="popover" data-content=""></i>
                </div>
                <div id="fontSize" class="analytic-detail mobile_seo">
                    @include('users.partials.result.loader_icon')
                </div>
            </div>
        </div>

        <div class="row css_border ">
            <div class="clearfix"></div>
            <div class="col-md-3 css_text factor mobile_seo padded_result ">
                Kích cỡ nội dung
                <i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="nội dung cần hiển thị vừa vặn với màn hình điện thoại và người dùng không cần kéo sang ngang để xem đầy đủ nội dung."></i>
            </div>
            <div class="col-md-9 bd padded_result">
                <div class="report-icon">
                    <i class="fa fa-info-circle" aria-hidden="true" data-toggle="popover" data-content=""></i>
                </div>
                <div id="scrollWidth" class="analytic-detail mobile_seo">
                    @include('users.partials.result.loader_icon')
                </div>
            </div>
        </div>

        <div class="row css_border ">
            <div class="clearfix"></div>
            <div class="col-md-3 css_text factor mobile_seo padded_result ">
                Hình ảnh
                <i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="Xem website hiển thị như thế nào trên thiết bị di động"></i>
            </div>
            <div class="col-md-9 bd padded_result">
                <div class="report-icon">
                    <i class="fa fa-info-circle" aria-hidden="true" data-toggle="popover" data-content=""></i>
                </div>
                <div id="mobileSnapshoot" class="analytic-detail mobile_seo" style="position: relative;left: 0;">
                    @include('users.partials.result.loader_icon')
                </div>
            </div>
        </div>
    </div>
</section>