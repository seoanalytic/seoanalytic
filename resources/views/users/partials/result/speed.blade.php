<section id="speed">
    <div class="container">
        <div class="row ">
            <div class="col-md-12 title_css padded_result">Tốc độ tải trang</div>
        </div>

        <div class="row css_border">
            <div class="clearfix"></div>
            <div class="col-md-3 css_text factor speed_seo padded_result ">
                Thời gian tải của webpage
                <i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="Kiểm tra tốc độ tải trang web của bạn. Tốc độ trang là một yếu tố quan trọng trong bảng xếp hạng của công cụ tìm kiếm và thành công trang web tổng thể. Các trang mất nhiều thời gian hơn 5 giây để tải có thể làm giảm đến 50% người dùng. Trang web nhanh hơn dẫn đến lưu lượng truy cập cao hơn, chuyển đổi tốt hơn và tăng doanh số bán hàng trên các trang tải chậm hơn."></i>
            </div>
            <div class="col-md-9 bd padded_result">
                <div class="report-icon">
                    <i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>
                </div>
                <div id="websiteLoadTime" class="analytic-detail speed_seo">
                    @include('users.partials.result.loader_icon')
                </div>
            </div>
        </div>

        <div class="row css_border">
            <div class="clearfix"></div>
            <div class="col-md-3 css_text factor speed_seo padded_result ">
                Kích thước trang HTML
                <i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="Kiểm tra kích thước HTML của trang của bạn. Kích thước HTML là kích thước của tất cả các mã HTML trên trang web của bạn - kích thước này không bao gồm hình ảnh, javascripts bên ngoài hoặc các tệp CSS bên ngoài"></i>
            </div>
            <div class="col-md-9 bd padded_result">
                @if($isGzipEnabled)
                    @if( (round($gzPageSize/1024,2) < 33) || (round($HTMLPageSize/1024,2) < 33) )
                        <div class="report-icon">
                            <i class="fa fa-check-circle-o" aria-hidden="true" data-toggle="popover" data-content="OK"></i>
                        </div>
                    @else
                        <div class="report-icon">
                            <i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>
                        </div>
                    @endif
                @else
                    @if( round($HTMLPageSize/1024,2) < 33 )
                        <div class="report-icon">
                            <i class="fa fa-check-circle-o" aria-hidden="true" data-toggle="popover" data-content="OK"></i>
                        </div>
                    @else
                        <div class="report-icon">
                            <i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>
                        </div>
                    @endif
                @endif
                <div class="analytic-detail speed_seo">
                    Kích thước HTML của trang web của bạn là
                    @if($isGzipEnabled)
                        <b>{{ round($gzPageSize/1024,2) }} KB </b><br/>
                        @if( (round($gzPageSize/1024,2) < 33) || (round($HTMLPageSize/1024,2) < 33) )
                            <br/>Trang web tải nhanh hơn mang lại trải nghiệm người dùng tốt hơn, tỷ lệ chuyển đổi và xếp hạng cao hơn với các công cụ tìm kiếm với kích thước HTML < 33 Kb.
                            @else
                            <br>Kích thước HTML của trang web trung bình là <strong>33 KB</strong>.

                            <button type="button" class="mar-t btn-sweet" data-toggle="modal" data-target="#myModal">Khắc phục</button>
                                <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-vertical-alignment-helper">
                                        <div class="modal-dialog modal-vertical-align-center">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title"><b>Hướng dẫn - Giảm kích thước HTML</b></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Để giải quyết vấn đề này, bạn nên:</p>
                                                    <ul>
                                                        <li>Sử dụng Gzip để nén dữ liệu.</li>
                                                        <li>Di chuyển tất cả các file CSS thành 1 file, nhúng file CSS ngoại tuyến vào HTML và minify file CSS.</li>
                                                        <li>Minify tất cả các file JS và nhúng mã ngoại tiếng vào file HTML.</li>
                                                        <li>Sử dụng layout CSS thay vì sử dụng table.</li>
                                                    </ul>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default button-close" data-dismiss="modal">Đóng</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                        @endif
                    @else
                        <b>{{ round($HTMLPageSize/1024,2) }} Kb </b><br/>
                        @if( (round($HTMLPageSize/1024,2) < 33) )
                            <br/>Trang web tải nhanh hơn mang lại trải nghiệm người dùng tốt hơn, tỷ lệ chuyển đổi và xếp hạng cao hơn với các công cụ tìm kiếm với kích thước HTML < 33 Kb.
                            @else
                            Kích thước HTML của trang web trung bình là <strong>33 Kb</strong>.
                            <button type="button" class="mar-t btn-sweet" data-toggle="modal" data-target="#myModal">Khắc phục</button>
                            <div class="modal fade" id="myModal" role="dialog">
                                <div class="modal-vertical-alignment-helper">
                                    <div class="modal-dialog modal-vertical-align-center">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title"><b>Hướng dẫn - Giảm kích thước HTML</b></h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Để giải quyết vấn đề này, bạn nên:</p>
                                                <ul>
                                                    <li>Sử dụng Gzip để nén dữ liệu.</li>
                                                    <li>Di chuyển tất cả các file CSS thành 1 file, nhúng file CSS ngoại tuyến vào HTML và minify file CSS.</li>
                                                    <li>Minify tất cả các file JS và nhúng mã ngoại tiếng vào file HTML.</li>
                                                    <li>Sử dụng layout CSS thay vì sử dụng table.</li>
                                                </ul>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default button-close" data-dismiss="modal">Đóng</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>

        <div class="row css_border">
            <div class="clearfix"></div>
            <div class="col-md-3 css_text factor speed_seo padded_result ">
                Nén file HTML/GZIP
                <i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="Kiểm tra xem trang web của bạn đang sử dụng HTML compression hay không. Việc nén HTML đóng một vai trò quan trọng trong việc cải thiện tốc độ trang web bằng cách tìm kiếm chuỗi tương tự trong một tập tin văn bản và thay thế chúng tạm thời để giảm kích thước tập tin tổng thể."></i>
            </div>
            <div class="col-md-9 bd padded_result">
                @if($isGzipEnabled)
                    <div class="report-icon">
                        <i class="fa fa-check-circle-o" aria-hidden="true" data-toggle="popover" data-content="OK"></i>
                    </div>
                    <div class="analytic-detail speed_seo">
                        Chúc mừng! Trang web của bạn có sử dụng <b>gzip compression</b>.<br/>
                        Tập tin HTML gốc là
                        <b>{{ round($HTMLPageSize/1024,2) }}KB </b> sau khi được nén còn <b>{{ round($gzPageSize/1024,2) }} KB</b>
                        (giảm <b>{{ round(100-($gzPageSize/$HTMLPageSize)*100,0) }} %</b> kích thước). <br/>
                        Điều này giúp đảm bảo tải trang web nhanh hơn và trải nghiệm người dùng được cải thiện.
                    </div>
                @else
                    <div class="report-icon">
                        <i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>
                    </div>
                    <div class="analytic-detail speed_seo">
                        Trang không sử dụng <b>gzip</b>
                        <button type="button" class="mar-t btn-sweet" data-toggle="modal" data-target="#GZIPmyModal">Khắc phục</button>
                        <div class="modal fade" id="GZIPmyModal" role="dialog">
                            <div class="modal-vertical-alignment-helper">
                                <div class="modal-dialog modal-vertical-align-center">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title"><b>Khắc phục - Nén file HTML/GZIP</b></h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Có 2 lựa chọn cho việc nén file đó là <strong>Deflate</strong> và <strong>GZIP</strong>:</p>
                                            <ul>

                                                <li><strong>GZIP</strong> sẽ đạt được tỉ lệ nén cao hơn và là một lựa chọn tốt hơn nếu trang web bạn sử dụng nhiều hình ảnh hoặc nhiều kích thước file lớn. Bạn có thể đọc thêm cách <a target="_blank" href="https://varvy.com/pagespeed/enable-compression.html">cài đặt</a></li>
                                                <li><strong>Deflate</strong> là tùy chọn tự động đi kèm với server Apache và nó đơn giản để cài đặt.</li>
                                                Thiết lập nén file cho website của bạn tùy thuộc vào loại máy chủ bạn đang sử dụng cho trang web của bạn.
                                                Nếu sử dụng Apache, bạn cần kích hoạt tính năng nén bằng cách thêm một vài mã deflate vào tệp tin .htaccess của bạn
                                                <pre>
 # compress text, html, javascript, css, xml:
 AddOutputFilterByType DEFLATE text/plain
 AddOutputFilterByType DEFLATE text/html
 AddOutputFilterByType DEFLATE text/xml
 AddOutputFilterByType DEFLATE text/css
 AddOutputFilterByType DEFLATE application/xml
 AddOutputFilterByType DEFLATE application/xhtml+xml
 AddOutputFilterByType DEFLATE application/rss+xml
 AddOutputFilterByType DEFLATE application/javascript
 AddOutputFilterByType DEFLATE application/x-javascript
                                                 </pre>
                                            </ul>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default button-close" data-dismiss="modal">Đóng</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>

        <div class="row css_border">
            <div class="clearfix"></div>
            <div class="col-md-3 css_text factor speed_seo padded_result ">
                Doctype Test
                <i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="Để trình duyệt web biết đang sử dụng ngôn ngữ đánh dấu nào. Không chọn hoặc chọn nhầm DOCTYPE sẽ khiến các Web Browser xem như là Web Document được viết theo kiểu cũ, viết sai. Theo đó, các Web Browser sẽ xử lý trang Web làm sao đó để nó tương thích ngược với các phiên bản của các trình duyệt này, và làm theo cách riêng của chúng. Vì thế, Web Documents sẽ được hiển thị khác nhau trên các trình duyệt khác nhau. Việc này khiến cho tốc độ tải trang lâu"></i>
            </div>
            <div class="col-md-9 bd padded_result">
                @if($doctype)
                    <div class="report-icon">
                        <i class="fa fa-check-circle-o" aria-hidden="true" data-toggle="popover" data-content="OK"></i>
                    </div>
                    <div class="analytic-detail speed_seo">
                        Trang web có khai báo <b>doctype</b>.
                    </div>
                @else

                    <div class="report-icon">
                        <i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>
                    </div>
                    <div class="analytic-detail speed_seo">
                        Trang web không khai báo <b>doctype</b>.
                        <button type="button" class="mar-t btn-sweet" data-toggle="modal" data-target="#doctypemyModal">Khắc phục</button>
                        <div class="modal fade" id="doctypemyModal" role="dialog">
                            <div class="modal-vertical-alignment-helper">
                                <div class="modal-dialog modal-vertical-align-center">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title"><b>Khắc phục - doctype</b></h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Bạn cần khai báo doctype dòng đầu tiên, trước thẻ <code>html</code>:</p>
                                            <strong>HTML5</strong>: <code>&lt;!DOCTYPE HTML&gt;</code>
                                            <br><p>Và còn một số loại Doctype bạn có thể tham khảo tại <a href="https://www.w3schools.com/tags/tag_doctype.asp" target="_blank">W3C.</a></p>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default button-close" data-dismiss="modal">Đóng</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>

        <div class="row css_border">
            <div class="clearfix"></div>
            <div class="col-md-3 css_text factor speed_seo padded_result ">
                Flash
                <i class="fa fa-question-circle-o" aria-hidden="true" data-toggle="popover" data-content="Việc kiểm tra webpage có chứa không sở dĩ Flash rất nặng cho nên việc sử dụng Flash làm giảm tốc độ tải trang"></i>
            </div>
            <div class="col-md-9 bd padded_result">
                @if($isUsingFlash)
                    <div class="report-icon">
                        <i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>
                    </div>
                    <div class="analytic-detail speed_seo">
                        Trang có sử dụng Flash.<br/>
                        Nội dung Flash không hoạt động tốt trên thiết bị di động.
                        {{----}}
                        <button type="button" class="mar-t btn-sweet" data-toggle="modal" data-target="#FlashmyModal">Khắc phục</button>
                        <div class="modal fade" id="FlashmyModal" role="dialog">
                            <div class="modal-vertical-alignment-helper">
                                <div class="modal-dialog modal-vertical-align-center">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title"><b>Khắc phục - Flash</b></h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Lí do website bạn không nên sử dụng Flash:</p>
                                            <ul>
                                                <li>Thiếu bảo mật</li>
                                                <li>Không làm việc tốt trên thiết bị di động</li>
                                                <li>Một lí do nữa là Flash làm trang web bạn tải chậm hơn</li>
                                            </ul>
                                            <p>Bạn có thể thay thế Flash bằng Canvas của HTML5</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default button-close" data-dismiss="modal">Đóng</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{----}}
                    </div>
                @else
                    <div class="report-icon">
                        <i class="fa fa-check-circle-o" aria-hidden="true" data-toggle="popover" data-content="OK"></i>
                    </div>
                    <div class="analytic-detail speed_seo">
                        Trang web không có <b>flash</b> (một công nghệ đã lỗi thời).
                    </div>
                @endif
            </div>
        </div>
    </div>
</section>