<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnalyzeHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analyze_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->text('url')->default('');
            $table->mediumText('html_content')->default('');
            $table->foreign('user_id')->references('id')->on('dpy_user');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analyze_history');
    }
}
