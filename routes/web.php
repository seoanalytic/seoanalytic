<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('scrape','WebScrapperController@index');

Route::group([], function () {
    Route::get('/', 'UsersController@index')->name('users.index');
    Route::post('/register', 'UsersController@register')->name('users.register');
    Route::post('/login', 'UsersController@login')->name('users.login');
    Route::get('/lich-su-tra-cuu', 'UsersController@lookupHistory')->name('users.lookup_history');
    Route::get('/thong-tin-ca-nhan', 'UsersController@profile')->name('users.profile');
    Route::post('/updateProfile', 'UsersController@updateProfile')->name('users.updateProfile');
    Route::get('/logout', 'UsersController@logout')->name('users.logout');
    Route::get('/du-lieu', 'WebScrapperController@index')->name('check.index');
    Route::get('/lien-he', 'ContactController@contact')->name('branch.contact');
    Route::post('/lien-he', 'ContactController@sendFaq')->name('branch.send_faq');
    Route::get('/dem-ky-tu', 'ToolController@word')->name('tool.word');
    Route::get('/resize-img', 'ToolController@getResizeImage')->name('tool.getresizeimage');
    Route::post('/resize-img','ToolController@postResizeImage')->name('tool.postresizeimage');
    Route::get('/tao-sitemap','ToolController@sitemap')->name('tool.sitemap');
    Route::get('/tao-favicon','ToolController@favicon')->name('tool.favicon');
    Route::post('/tao-favicon','ToolController@postFavicon')->name('tool.postfavicon');
});


/*Route::get('/', function () {
    return view('home');
});*/

Route::get('/home', 'HomeController@index');

Route::group(['prefix' => 'admin'], function () {
	
	Auth::routes();
 	Route::get('/', 'HomeController@index');
 	Route::get('logout', function () {
        Auth::logout();
        Session::flush();
        return Redirect::to('/admin');
    });

    // Home 
    Route::get('/dashboard', 'HomeController@index')->name('admin.home.index');

    //FAQs routes
    Route::group(['prefix' => 'hoi-dap'], function () {
        Route::get('/', ['as' => 'admin.faq.index', 'uses' => 'FAQsController@index']);
        Route::get('cap-nhat/{id}', ['as' => 'admin.faq.edit', 'uses' => 'FAQsController@edit']);
        Route::get('xoa/{id}', ['as' => 'admin.faq.del', 'uses' => 'FAQsController@delete']);
        Route::post('gui-mail', ['as' => 'admin.faq.email', 'uses' => 'FAQsController@email']);
    });

    Route::group(['prefix' => 'nguoi-dung'], function () {
        Route::get('/', ['as' => 'admin.user.index', 'uses' => 'AdminUsersController@index']);
        Route::get('cap-nhat/{id}', ['as' => 'admin.user.edit', 'uses' => 'AdminUsersController@edit']);
        Route::POST('updateProfile/{id}', ['as' => 'admin.user.updateProfile', 'uses' => 'AdminUsersController@updateProfile']);
        Route::get('xoa/{id}', ['as' => 'admin.user.del', 'uses' => 'AdminUsersController@delete']);
    });
});

Route::group(['prefix' => 'seo-analyst'], function () {
    Route::post('mobileSnapshoot', 'MobileController@mobileSnapshoot');
    Route::post('mobileFriendly', 'MobileController@mobileFriendly');
    Route::post('websiteLoadTime', 'MobileController@websiteLoadTime');
    Route::post('storeAnalyzeHistory', 'WebScrapperController@storeAnalyzeHistory');

    Route::post('html2pdf', 'WebScrapperController@html2pdf');
});

Route::get('/html2pdfView', 'WebScrapperController@html2pdfView');
Route::get('/html2pdf', 'WebScrapperController@html2pdf');
