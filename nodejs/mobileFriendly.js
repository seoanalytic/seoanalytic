var system = require('system');
var args = system.args;

var userAgent = 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1';

var page = require('webpage').create();
var url = args[1];

page.viewportSize = {
    width: 320,
    height: 568
};

page.settings.userAgent = userAgent;

page.open(url, function(status) {
    var mobile_friendly_result = page.evaluate(function(){
        var mobile_friendly_obj = {
            'viewportWidth': Math.max(window.outerWidth,window.innerWidth),
            'scrollWidth' : Math.max(document.body.clientWidth,document.body.scrollWidth),
            'fontSize': window.getComputedStyle(document.body).getPropertyValue('font-size').substring(0,2),
            'metaViewportTag' : ''
        };

        if(document.querySelectorAll('meta[name="viewport"]').length > 0)
        {
            mobile_friendly_obj['metaViewportTag']=true;
        }
        else
        {
            mobile_friendly_obj['metaViewportTag']=false;
        }

        return mobile_friendly_obj;
    });
    console.log(JSON.stringify(mobile_friendly_result));
    phantom.exit();
});