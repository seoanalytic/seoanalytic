
const Pageres = require('pageres');
const uuidv4 = require('uuid/v4');
const filename = uuidv4();
const args = process.argv;
var userAgent = 'Mozilla/5.0 (iPhone; CPU iPhone OS 9_1 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13B143 Safari/601.1';
var url = args[2];
const pageres = new Pageres({delay: 2})
    .src(url, ['iphone 5s'], {crop: true,filename:filename, userAgent:userAgent})
    .dest(__dirname+'/../public/mobile-snapshoot')
    .run()
    .then(() => console.log(filename+'.png'));

