<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CheckRepository
 * @package namespace App\Repositories;
 */
interface CheckRepository extends RepositoryInterface
{
    //
}
