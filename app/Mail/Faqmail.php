<?php

namespace App\Mail;

use App\Models\DpyQuestion;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FaqMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $faq;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(DpyQuestion $faq)
    {
        //
        $this->faq = $faq;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'SEO Analytics - Trả lời hỏi đáp';
        //return $this->view('view.name');
        return $this->view('emails.faq_email')
            ->with([
                'fullname' => $this->faq->fullname,
                'email_questions' => $this->faq->email_questions,
                'content_questions' => $this->faq->content_questions,
                'reply_questions' => $this->faq->reply_questions,
            ])
            ->subject($subject);
    }
}
