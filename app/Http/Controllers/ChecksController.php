<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\CheckCreateRequest;
use App\Http\Requests\CheckUpdateRequest;
use App\Repositories\CheckRepository;
use App\Validators\CheckValidator;


class ChecksController extends Controller
{

    /**
     * @var CheckRepository
     */
    protected $repository;

    public function __construct(CheckRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.pages.analytic_result');
    }

    // check exists website
    public function get_header($url)
	{
		$file_headers = @get_headers($url);
     	if (strpos($file_headers[0], "200 OK") > 0) 
     	{
        	return true;
      	} 
      	else 
      	{
        	return false;
      	}
	}
	function getDomain($url)
	{
	    if(filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_HOST_REQUIRED) === FALSE)
	    {
	        return false;
	    }
	    /*** get the url parts ***/
	    $parts = parse_url($url);
	    /*** return the host domain ***/
	    return $parts['scheme'].'://'.$parts['host'];
	}
}
