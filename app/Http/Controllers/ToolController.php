<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
use App\Helper\Sitemap;
use Log;

class ToolController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function word()
    {
        return view('users.pages.tools_word_count');
    }

    public function getResizeImage()
    {
        return view('users.pages.tools_resize_img');
    }
    public function postResizeImage(Request $request)
    {
        $this->validate($request, [
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1024',
        ]);
        $photo = $request->file('photo');
        $imagename = time().'.'.$photo->getClientOriginalExtension(); 
   
        $destinationPath = public_path('/thumbnail_images');
        $i = $request->input('width'); 
        $j = $request->input('height');
        $thumb_img = Image::make($photo->getRealPath())->resize($i, $j);
        $thumb_img->save($destinationPath.'/'.$imagename,80);
                    
        $destinationPath = public_path('/normal_images');
        $photo->move($destinationPath, $imagename);
        return back()
            ->with('success','Upload ảnh thành công')
            ->with('imagename',$imagename);
    }

    public function sitemap()
    {
        return view('users.pages.tools_sitemap');
    }

    public function postFavicon(){
        $generate_favicon = ['code' => 0, 'msg' =>  'Error!'];
        // Create favicon.
        $postvars = array(
            "image"             => trim($_FILES["image"]["name"]),
            "image_tmp"         => $_FILES["image"]["tmp_name"],
            "image_size"        => (int)$_FILES["image"]["size"],
            "image_dimensions"  => (int)$_POST["image_dimensions"]);

        // Provide valid extensions and max file size
        $valid_exts = array("jpg","jpeg","gif","png");
        $max_file_size = 179200; // 175kb

        $filenameParts = explode(".",$postvars["image"]);
        $ext = strtolower(end($filenameParts));
        $directory = public_path('\favicon\\'); // Directory to save favicons. Include trailing slash.
        $rand = rand(1000,9999);
        $filename = $rand.$postvars["image"];

        // Check not larger than max size.
        if($postvars["image_size"] <= $max_file_size){
            // Check is valid extension.
            if(in_array($ext,$valid_exts)){
                if($ext == "jpg" || $ext == "jpeg"){
                    $image = imagecreatefromjpeg($postvars["image_tmp"]);
                }
                else if($ext == "gif"){
                    $image = imagecreatefromgif($postvars["image_tmp"]);
                }
                else if($ext == "png"){
                    $image = imagecreatefrompng($postvars["image_tmp"]);
                }
                if($image){
                    list($width,$height) = getimagesize($postvars["image_tmp"]);
                    $newwidth = $postvars["image_dimensions"];
                    $newheight = $postvars["image_dimensions"];
                    $tmp = imagecreatetruecolor($newwidth,$newheight);
                        
                    // Copy the image to one with the new width and height.
                    imagecopyresampled($tmp,$image,0,0,0,0,$newwidth,$newheight,$width,$height);
                
                    // Create image file with 100% quality.
                    if(is_dir($directory)){
                        if(is_writable($directory)){
                            imagejpeg($tmp,$directory.$filename,100) or die('Could not make image file');
                            if(file_exists($directory.$filename)){  
                                // Image created, now rename it to its
                                $ext_pos = strpos($rand.$postvars["image"],"." . $ext);
                                $strip_ext = substr($rand.$postvars["image"],0,$ext_pos);
                                // Rename image to .ico file
                                rename($directory.$filename,$directory.$strip_ext.".ico");
                                $generate_favicon =   ['code' => 1, 'msg' =>  $strip_ext];
                            } else {
                                $generate_favicon =  ['code' => 0, 'msg' =>  'File không được tạo.'];
                            }
                        } else {
                            $generate_favicon =  ['code' => 0, 'msg' =>  'Đường dẫn không thể tạo.'];
                        }
                    } else {
                        $generate_favicon =  ['code' => 0, 'msg' =>  'Đường dẫn không hợp lệ.'];
                    }
                
                    imagedestroy($image);
                    imagedestroy($tmp);
                } else {
                    $generate_favicon =  ['code' => 0, 'msg' =>  'Không thể tạo tệp hình ảnh.'];
                }
            } else {
                $generate_favicon =  ['code' => 0, 'msg' =>  'Kích thước tệp quá lớn. Kích thước tệp tối đa cho phép là 175kb.'];
            }
        } else {
            $generate_favicon =  ['code' => 0, 'msg' =>  'Loại tệp không hợp lệ. Bạn phải tải lên tệp hình ảnh. (jpg, jpeg, gif, png).'];
        }
        return view('users.pages.tools_favicon', compact('generate_favicon'));
    }

    public function favicon()
    {
        $generate_favicon=null;  
        return view('users.pages.tools_favicon', compact('generate_favicon'));
    }

}
