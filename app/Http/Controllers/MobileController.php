<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MobileController extends Controller
{
    protected $nodeJSPath = 'E:\\Graduation\\seo\\source\\nodejs';
    //protected $nodeJSPath = 'E:\\seoanalytic\\nodejs';
    public function mobileSnapshoot(Request $request)
    {
        $url = $request->input('url');
        if($request->input('url'))
        {
            $function = 'node';
            $jsFile= '\\mobileSnapshoot.js';
            $response = shell_exec($function.' '.$this->nodeJSPath.$jsFile.' '.$url);
            return $response;
        }
    }

    public function mobileFriendly(Request $request)
    {
        $url = $request->input('url');
        if($request->input('url'))
        {
            $function = 'phantomjs';
            $jsFile= '\\mobileFriendly.js';
//            $command = $function.' '.$this->nodeJSPath.$jsFile.' '.$url;
//            echo $command;
//            exit;
            $response = exec($function.' '.$this->nodeJSPath.$jsFile.' '.$url);

            return $response;
//            echo 'why?';
        }
    }

    public function websiteLoadTime(Request $request){
        $url = $request->input('url');
        if($request->input('url'))
        {
            $function = 'phantomjs';
            $jsFile= '\\phantomjs\\examples\\loadspeed.js';
//            $command = $function.' '.$this->nodeJSPath.$jsFile.' '.$url;
//            echo $command;
//            exit;
            $response = exec($function.' '.$this->nodeJSPath.$jsFile.' '.$url);

            return $response;
//            echo 'why?';
        }
    }
}
