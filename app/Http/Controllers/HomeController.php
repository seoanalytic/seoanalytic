<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\FAQRepository;


class HomeController extends Controller
{
    protected $repository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(FAQRepository $repository)
    {
        $this->middleware('auth');
        $this->repository = $repository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->repository->all();
        return view('admin.pages.dashboard', ['data' => $data]);
    }
}
