<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use App\Models\DpyUsers;
use Validator;

class AdminUsersController extends Controller
{
    //
    protected $repository;
    public function __construct(UserRepository $repository )
    {
        $this->repository = $repository;
    }

    public function index()
    {

        $data = $this->repository->all();
        return view('admin.pages.user.user_list', ['data' => $data]);
    }

     public function edit($id)
    {

        $data = $this->repository->find($id)->toArray();

        return view('admin.pages.user.user_edit')->with('data', $data);
    }

    public function updateProfile($id, Request $request){
        $messages = [
            'admin_user_email.required'    => 'Địa chỉ email không được bỏ trống.',
            'admin_user_email.max'         => 'Địa chỉ email có tối đa 90 ký tự',
            'admin_user_email.email'       => 'Địa chỉ email không đúng định dạng.',
            'admin_user_name.required'        => 'Họ và tên không được bỏ trống.',
            'admin_user_name.max'             => 'Họ và tên có tối đa 128 ký tự.',
            'admin_user_created_at.required'        => 'Ngày tạo không được bỏ trống.',
            'admin_user_created_at.date_format'        => 'Ngày giờ không dúng định dạng.',
            'admin_user_updated_at.required'        => 'Ngày cập nhật lần cuối không được bỏ trống.',
            'admin_user_updated_at.date_format'        => 'Ngày giờ không dúng định dạng.',
        ];
        $validator = Validator::make($request->toArray(), [
            'admin_user_name'         => 'required|max:128',
            'admin_user_email'    => 'required|max:90|email',
            'admin_user_created_at' => 'required|date_format:"Y-m-d H:i:s"',
            'admin_user_updated_at' => 'required|date_format:"Y-m-d H:i:s"',
        ], $messages);

        if ($validator->fails() == null) {
            $customer = $request->all();

            $customer['name'] = $customer['admin_user_name'];
            $customer['email'] = $customer['admin_user_email'];
            $customer['created_at'] = $customer['admin_user_created_at'];
            $customer['updated_at'] = $customer['admin_user_updated_at'];

            $dpy_user = DpyUsers::where('id_users', '=', $id)->first();
            if (!empty($dpy_user))
            {
                $customer = array_except($customer, array('password', 'history'));
                $customer = $this->repository->update($customer,$id);

                return response()->json(['data' => $customer]);
            }
            else
                return response()->json(['errors' => $validator->errors()]);
        }
        return response()->json(['errors' => $validator->errors()]);
    }

    public function delete($id)
    {
        $response = ['result' => 1,
                     'title' => __('messages.title_delete_success'),
                     'text' => __('messages.text_delete_success', ['name_object' => __('messages.user')])];
        try {
            $this->repository->delete($id);
        } catch (\Exception $e) {
            $msg = 'Xảy ra lỗi.';
            $response['result'] = 0;
            $response['title'] = __('messages.title_delete_fail');
            $response['text'] = __('messages.text_delete_fail', ['name_object' => __('messages.user')]);
        }
        // 1: success, 0: error -> fail
        return response()->json($response);
    }
}
