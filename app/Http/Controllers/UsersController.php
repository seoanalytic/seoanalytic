<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Models\DpyUsers;
use App\Models\AnalyzeHistory;
use Validator;
use Session;
use Illuminate\Support\Facades\Hash;



class UsersController extends Controller
{
    //
	protected $repository;
    public function __construct(UserRepository $repository )
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return view('users.pages.index');
    }

    //Route::post('/register','customer.register','CustomerController@register']);
    public function register(Request $request)
    {
    	$messages = [
            'name.required'        => 'Họ và tên không được bỏ trống.',
            'name.max'             => 'Họ và tên có tối đa 128 ký tự.',
            'email.required'       => 'Địa chỉ email không được bỏ trống.',
            'email.max'            => 'Địa chỉ email có tối đa 90 ký tự',
            'email.email'          => 'Địa chỉ email không đúng định dạng.',
            'email.unique'         => 'Địa chỉ email này đã tồn tại.',
            'password.required'    => 'Mật khẩu không được bỏ trống.',
            'password.max'         => 'Mật khẩu có tối đa 20 ký tự.',
            'repeatpass.required'  => 'Nhập lại mật khẩu không được bỏ trống.',
            'repeatpass.max'       => 'Nhập lại mật khẩu có tối đa 20 ký tự.',
        ];
        $validator = Validator::make($request->toArray(), [
        	'name'         => 'required|max:128',
            'email'        => 'required|max:90|email|unique:dpy_user',
            'password'     => 'required|max:20',
            'repeatpass'   => 'required|max:20',
        ], $messages);

        if ($validator->fails() == null) {
            $customer = $request->all();
            $customer["password"] = Hash::make($customer["password"]);
            $customer = $this->repository->create($customer);
            $customer = array_except($customer, ['password']);
            session(['user' => $customer]);
            return response()->json(['data' => $customer]);
        }
        return response()->json(['errors' => $validator->errors()]);
    }

    //Route::post('/login','customer.login','CustomerController@login']);
    public function login(Request $request)
    {
    	$messages = [
            'login_email.required'    => 'Địa chỉ email không được bỏ trống.',
            'login_email.max'         => 'Địa chỉ email có tối đa 90 ký tự',
            'login_email.email'       => 'Địa chỉ email không đúng định dạng.',
            'login_email.exists'      => 'Địa chỉ email không tồn tại.',
            'login_password.required' => 'Mật khẩu không được bỏ trống.',
            'login_password.max'      => 'Mật khẩu có tối đa 20 ký tự.',
        ];
        $validator = Validator::make($request->toArray(), [
            'login_email'    => 'required|max:90|email|exists:dpy_user,email',
            'login_password' => 'required|max:20',
        ], $messages);
        if ($validator->fails() == null){
            $request->all();
            
            $password = $request['login_password'];
                       
            $customer = $this->repository->findWhere(['email' => $request->login_email])->first();

            if (Hash::check($password, $customer->password)) {
                $customer = array_except($customer, ['password']);
                session(['user' => $customer]);
                return response()->json(['data' => $customer]);
            }
            $arrayFail = array('login_password' =>'Mật khẩu không đúng.');
            return response()->json(['errors' => $arrayFail]);
        }
        return response()->json(['errors' => $validator->errors()]);
    }
    //logout
    public function logout(){
    	session()->flush();
    	return redirect()->route('users.index');
    }

    public function lookupHistory(){
        $session_user = session('user');
        if($session_user != null) {
            $dpy_user = DpyUsers::where('email', '=', $session_user->email)->first();
            $current_history = AnalyzeHistory::where('user_id','=',$dpy_user->id_users)
                                ->orderBy('created_at','desc')
                                ->get();

            if($current_history == NULL)
                return view('users.pages.lookup_history')->with('lookup_history',NULL);                

            // $lookup_history = explode('|',$current_history);

            return view('users.pages.lookup_history')->with('lookup_history',$current_history);
        }
        return view('users.pages.index');
    }

    public function profile(){
        $session_user = session('user');
        if($session_user != null) {
            $dpy_user = DpyUsers::where('email', '=', $session_user->email)->first();
            if ($dpy_user !=null){

                $user_info = array();

                $user_info['name'] = $dpy_user->name;
                $user_info['email'] = $dpy_user->email;
                $user_info['created_at']= $dpy_user->created_at->toDateTimeString();

                $created_at = date_create_from_format('Y-m-d H:i:s', $user_info['created_at']);

                $user_info['created_at'] = $created_at->format('H:i:s d-m-Y');

                return view('users.pages.profile')->with('user_info',$user_info);
            }
            return view('users.pages.index');
        }
        return view('users.pages.index');
    }

    public function updateProfile(Request $request){
        $session_user = session('user');
        if($session_user != null) {
            $messages = [
                'profile_name.required'        => 'Họ và tên không được bỏ trống.',
                'profile_name.max'             => 'Họ và tên có tối đa 128 ký tự.',
            ];
            $validator = Validator::make($request->toArray(), [
                'profile_name'         => 'required|max:128'
            ], $messages);

            if ($validator->fails() == null) {
                $customer = $request->all();
                $customer['name']=$customer['profile_name'];
                $dpy_user = DpyUsers::where('email', '=', $session_user->email)->first();
                $customer_id = $dpy_user->id_users;
                
                $customer = array_except($customer, array('email', 'password','created_at'));
                $customer = $this->repository->update($customer,$customer_id);
                $customer = array_except($customer, array('password','history'));
                session(['user' => $customer]);
                return response()->json(['data' => $customer]);
            }
            return response()->json(['errors' => $validator->errors()]);
        }
        else
        {
            return view('users.pages.index');
        }
    }
}