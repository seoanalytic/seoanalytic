<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\FaqRequest;
use App\Repositories\FAQRepository;
use Log;
use Validator;

class ContactController extends Controller
{
    //
    protected $repository;
    //protected $faq_repository;

    public function __construct(FAQRepository $repository)
    {
        $this->repository = $repository;
        //$this->faq_repository = $faq_repository;
    }


    public function sendFaq(FaqRequest $request)
    {
       $msg = '';
        try {
            $data = $request->toArray();
            $data["status_questions"] = 1;
            $data = $this->repository->create($data);
            if ($data) {
                $msg = 'Đã gửi thành công!';
            } else {
                $msg = 'Gửi thất bại.';
            }

        } catch (\Exception $e) {
            $msg = 'Xảy ra lỗi.';
        }
        return redirect()->route('users.index', ['msg' => $msg]);
    }
}
