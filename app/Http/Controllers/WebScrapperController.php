<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/* for read content of website*/
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;
use Log;
use PDF;
use Symfony\Component\BrowserKit\Response;

use GuzzleHttp\Exception\GuzzleException;

use App\Models\DpyUsers;
use App\Models\AnalyzeHistory;

use Illuminate\Support\Facades\Redis;

class WebScrapperController extends Controller
{
    public function index(Request $request)
    {
        if($request->input('url'))
        {
            $time_start = microtime(true);

            //gán url vào domain, nếu ko nhập thì default là trang đó
            $domain = $request->input('url','http://nc.uit.edu.vn/');
            $rawHTML = $this->sendCurlRequest($domain);
//            $client = new Client();
//            $crawler = $client->request('GET',$domain);
            $crawler = new Crawler($rawHTML);
            $url = array('url' => $domain);

            $title = array('title' => $crawler->filter('title')->text());
            $meta = array('meta' =>$crawler->filter('meta'));
            $link = array('link' =>$crawler->filter('link'));

            $h1 = array('h1' => $crawler->filter('h1'));
            $h2 = array('h2' => $crawler->filter('h2'));
            $h = array('h1' => $crawler->filter('h1'), 'h2' => $crawler->filter('h2'), 'h3' => $crawler->filter('h3'), 'h4' => $crawler->filter('h4'), 'h5' => $crawler->filter('h5'), 'h6' => $crawler->filter('h6'));

            $img = array('img' => $crawler->filter('img'));

            /*-----tính thời gian xử lý------*/
            $time_end = microtime(true);
            $time = $time_end - $time_start;
            $time = array('time' => $time);

            $rawHTML = $this->sendCurlRequest($domain);

            /*----HTML Page Size----*/
            $HTMLPageSize = strlen($rawHTML);

            // $gzClient = new \GuzzleHttp\Client();
            // $gzResponse = $gzClient->request('GET',$domain,[
            //     'headers' => ['Accept-Encoding' => 'gzip'],
            //     'decode_content' => null
            // ]);


            /*----End HTML Page Size----*/


            /*----Check gzip is enable----*/
            $isGzipEnabled = $this->isGzipEnable($domain);;
            /*----End check gzip is enable----*/


            /*----Caculate gzip page size----*/
            if($isGzipEnabled)
            {
                $gzPageSize = strlen(gzcompress($rawHTML));
            }

            /*----End caculate gzip page size----*/

            /*----Detect Flash----*/
            $flashObject = $crawler->filter('object');
            $flashEmbed = $crawler->filter('embed');
            if($flashObject->count() > 0 || $flashEmbed->count() > 0)
                $isUsingFlash = true;
            else
                $isUsingFlash = false;
            /*----End detect Flash----*/

            /*-----lưu lại lịch sử tra cứu------*/
            $session_user = session('user');
            $user_id = 0;
            if($session_user != null) {
                $dpy_user = DpyUsers::where('email', '=', $session_user->email)->first();
                    $new_history = $dpy_user->history;

                    if($new_history != null)
                    {
                        $history_array = explode('|',$new_history);
                        
                        //nếu URL chưa tra cứu thì thêm vô lịch sử
                        if(!in_array($domain,$history_array)){
                            $new_history = $dpy_user->history;
                            $new_history.='|'.$domain;
                        }
                    }
                    else
                    {
                        $new_history=$domain;
                    }
                    $dpy_user->history = $new_history;
                    $dpy_user->save();
                    $user_id = $dpy_user->id_users;
                }

            if(stripos($rawHTML,'<!doctype html') >-1)
            {
                $doctype = true;
            }
            else
            {
                $doctype = false;
            }
            
            $isCompare=false;
            $html_content='';
            $date_in_past = 0;
            if($request->input('compare_id') > 0)
            {
                $analyze_history_id = $request->input('compare_id');
                $isCompare=true;
                $analyze_history_result = AnalyzeHistory::where('id','=',$analyze_history_id)
                                ->first();
                if($analyze_history_result == NULL)
                {
                    redirect('/lich-su-tra-cuu');
                }
                else
                {
                    $html_content = $analyze_history_result->html_content;
                    $date_in_past = date('H:i:s d-m-Y', strtotime($analyze_history_result->created_at));
                }
            }

            return view('users.pages.analytic_result', compact([
                    'url', 'title','h1','h2', 'h', 'meta', 'img', 'time',
                    'HTMLPageSize','isGzipEnabled','gzPageSize','doctype','isUsingFlash','link','domain','user_id','isCompare','html_content','date_in_past']));
        }
        else
            return view('users.pages.index');
    }

    public function html2pdfView(Request $request)
    {
        if($request->input('html_content')) {
            $html_content = $request->input('html_content');
            return view('html2pdf.pdf_report',array('html_content'=>$html_content));
        }
    }

    public function html2pdf(Request $request)
    {
        if($request->input('html_content')) {

            $html_content = $request->input('html_content');
//            return var_dump($html_content);
            $filename = uniqid();
            $pdf = PDF::loadView('html2pdf.pdf_report',array('html_content'=>$html_content))->save('html2pdf/'.$filename.'.pdf');

            return $filename.'.pdf';
        }

    }

    public function storeAnalyzeHistory (Request $request)
    {
        $html_content = $request->input('html_content');
        $user_id = $request->input('user_id');
        $url = $request->input('url');
        $reponse = ['status' => 'failed'];

        if($html_content != '' && $user_id > 0 && $url != '')
        {
            $analyze_history = new AnalyzeHistory;
            $analyze_history->html_content = $html_content;
            $analyze_history->user_id = $user_id;
            $analyze_history->url = $url;
            if ($analyze_history->save())
            {
                $reponse = ['status' => 'success'];
            }
        }
        return json_encode($reponse);
    }

    public function sendCurlRequest($url,$getType = 'html')
    {
        $ch = @curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        $head[] = "Connection: keep-alive";

        $head[] = "Keep-Alive: 300";

        $head[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";

        $head[] = "Accept-Language: en-us,en;q=0.5";

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36");
        curl_setopt($ch, CURLOPT_ENCODING, '');

        curl_setopt($ch, CURLOPT_HTTPHEADER, $head);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        curl_setopt($ch, CURLOPT_TIMEOUT, 60);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(

            'Expect:'
        ));

        if($getType == 'header')
        {
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_setopt($ch, CURLOPT_HEADER, TRUE);
        }

        $curlResponse = curl_exec($ch);
        curl_close($ch);
        return $curlResponse;
    }

    function isGzipEnable($url)
    {
        $responseHeader = $this->sendCurlRequest($url,'header');
        $isGzipEnable = false;
        foreach (explode("\r\n",$responseHeader) as $header) {
            if(strtolower($header) == 'content-encoding: gzip')
            {
                $isGzipEnable = true;
                break;
            }
        }

        return $isGzipEnable;
    }

    public function backlink($url){
        // Get your access id and secret key here: https://moz.com/products/api/keys
        $accessID = "mozscape-2dbad0eb1e";
        $secretKey = "b2b0580e2ad34423837908d49737c8c6";
        // Set your expires times for several minutes into the future.
        // An expires time excessively far in the future will not be honored by the Mozscape API.
        $expires = time() + 300;
        // Put each parameter on a new line.
        $stringToSign = $accessID."\n".$expires;
        // Get the "raw" or binary output of the hmac hash.
        $binarySignature = hash_hmac('sha1', $stringToSign, $secretKey, true);
        // Base64-encode it and then url-encode that.
        $urlSafeSignature = urlencode(base64_encode($binarySignature));
        // Specify the URL that you want link metrics for.
        $objectURL = "www.daa.uit.edu.vn";
        // Add up all the bit flags you want returned.
        // Learn more here: https://moz.com/help/guides/moz-api/mozscape/api-reference/url-metrics
        $cols = "140737488355328";
        // Put it all together and you get your request URL.
        // This example uses the Mozscape URL Metrics API.
        $requestUrl = "http://lsapi.seomoz.com/linkscape/url-metrics/".urlencode($objectURL)."?Cols=".$cols."&AccessID=".$accessID."&Expires=".$expires."&Signature=".$urlSafeSignature;
        // Use Curl to send off your request.
        $options = array(
            CURLOPT_RETURNTRANSFER => true
            );
        $ch = curl_init($requestUrl);
        curl_setopt_array($ch, $options);
        $backlink = curl_exec($ch);
        curl_close($ch);
        return $backlink;
    }

}