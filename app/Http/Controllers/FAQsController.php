<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\FaqRequest;
use App\Repositories\FAQRepository;
use App\Mail\FaqMail;
use Validator;
use Mail;
use Auth;


class FAQsController extends Controller
{

    /**
     * @var FAQRepository
     */
    protected $repository;

    /**
     * @var FAQValidator
     */
    protected $validator;

    public function __construct(FAQRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = $this->repository->all();
        return view('admin.pages.faqs.faq_list', ['data' => $data]);
    }

    public function edit($id)
    {

        $data = $this->repository->find($id)->toArray();

        return view('admin.pages.faqs.faq_edit')->with('data', $data);
    }

    public function delete($id)
    {
        $response = ['result' => 1,
                     'title' => __('messages.title_delete_success'),
                     'text' => __('messages.text_delete_success', ['name_object' => __('messages.faq')])];
        try {
            $this->repository->delete($id);
        } catch (\Exception $e) {
            $msg = 'Xảy ra lỗi.';
            $response['result'] = 0;
            $response['title'] = __('messages.title_delete_fail');
            $response['text'] = __('messages.text_delete_fail', ['name_object' => __('messages.faq')]);
        }
        // 1: success, 0: error -> fail
        return response()->json($response);
    }

    public function email(FaqRequest $request)
    {
        $validator = Validator::make($request->toArray(), [
            'reply_questions' => 'required'
        ]);
        if ($validator->fails() == null) {

            $msg = ['error' => 0, 'msg' => ''];
            $id = $request->id_questions;
            if ($id != null) {
                try {
                    $request["status_questions"] = 2;
                    $request["id_user"] = Auth::user()->id;
                    $faq = $this->repository->update($request->toArray(), $id);
                    $mail = $faq->email_questions;

                    Mail::to($mail)->send(new FaqMail($faq));

                    $msg['msg'] = 'Thư đã phản hồi thành công đến khách hàng.';
                } catch (\Exception $e) {
                    $msg['msg'] = $e;//'Xảy ra lỗi.';
                    $msg['error'] = 1;
                }

            } else {
                $msg['msg'] = 'Không thể tìm thấy FAQ.';
                $msg['error'] = 1;
            }

            return view('admin.pages.faqs.faq_sendmail', compact('msg'));
        }else {
            return redirect(route('admin.faq.edit', $request->id_questions_feedback))
                ->withErrors($validator)
                ->withInput();
        }
    }
}