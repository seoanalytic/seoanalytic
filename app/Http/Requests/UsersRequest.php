<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersRequest extends FormRequest
{
    **
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|max:128',
            'email'         => 'required|max:45|unique:admins|email',
            'password'      => 'required|max:20|min:8|confirmed',
            'thumbnail'     => 'mimes:jpeg,bmp,png,jpg'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required'         => 'Tên admin không được bỏ trống.',
            'name.max'              => 'Tên admin có tối đa 128 ký tự.',

            'email.required'        => 'Email không được bỏ trống.',
            'email.max'             => 'Email có tối đa 45 ký tự',
            'email.email'           => 'Email không đúng định dạng.',
            'email.unique'          => 'Email này đã tồn tại.',

            'thumbnail.mimes'       => 'Hình đại diện không đúng định dạng hình ảnh.',
            
            'password.required'     => 'Mật khẩu không được bỏ trống.',
            'password.min'          => 'Mật khẩu có tối thiểu 8 ký tự.',
            'password.max'          => 'Mật khẩu có tối đa 20 ký tự',
            'password.confirmed'    => 'Mật khẩu không khớp'
        ];
    }
}
