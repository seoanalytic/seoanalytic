<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DpyQuestionsFeedback
 */
class DpyUsers extends Model
{
    protected $table = 'dpy_user';

    protected $primaryKey = 'id_users';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'email',
        'password',
        'history',
        'remember_token',
        'created_at	',
        'updated_at',
    ];

    protected $guarded = [];
}