<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DpyQuestionsFeedback
 */
class DBFaq extends Model
{
    protected $table = 'db_faq';

    protected $primaryKey = 'id_questions';

    public $timestamps = true;

    protected $fillable = [
        'fullname',
        'content_questions',
        'reply_questions',
        'email_questions',
        'created_day_questions',
        'replied_day_questions',
        'id_user',
        'status_questions'
    ];

    protected $guarded = [];

}