<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DpyQuestionsFeedback
 */
class DpyQuestion extends Model
{
    protected $table = 'dpy_questions';

    protected $primaryKey = 'id_questions';

    public $timestamps = true;

    protected $fillable = [
        'fullname',
        'content_questions',
        'reply_questions',
        'email_questions',
        'created_at	',
        'updated_at',
        'id_user',
        'status_questions'
    ];

    protected $guarded = [];
}