<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DpyQuestionsFeedback
 */
class Users extends Model
{
    protected $table = 'users';

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'name',
        'email',
        'password',
        'history',
        'remember_token',
        'created_at	',
        'updated_at',
    ];

    protected $guarded = [];
}