<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnalyzeHistory extends Model
{
    protected $table = 'analyze_history';

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'url',
        'html_content'
    ];

    protected $guarded = [];
}
