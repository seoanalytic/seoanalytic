/**  */
$(document).ready(function() {
    // Delete
    $('body').on('click' , '.btn-delete-swal', function() {
        var url = $(this).attr('value');
        var title = $(this).attr('data-title');
        var text = $(this).attr('data-text');

        swal({
            title: title,
            text: text,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Đồng ý!",
            cancelButtonText: "Không!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url: url,
                    type: "GET",
                    success: function(data){
                        if(data.result == 1) {
                            swal({
                                title: data.title,
                                text: data.text,
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            }, function(cfr) { if(cfr) window.location.reload(); });
                        }
                        else {
                            swal({
                                title: data.title,
                                text: data.text,
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            });  
                        }
                    }
                });
            }
        });
    });

    //
    // Update status news
    $('body').on('click' , '.btn-update-status', function() {
        var url = $(this).attr('value');
        var title = $(this).attr('data-title');
        var text = $(this).attr('data-text');
        
        swal({
            title: title,
            text: text,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Đồng ý!",
            cancelButtonText: "Không!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url: url,
                    type: "GET",
                    success: function(data){
                        if(data.result == 1) {
                            swal({
                                title: data.title,
                                text: data.text,
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            }, function(cfr) { if(cfr) window.location.reload(); });
                        }
                        else {
                            swal({
                                title: data.title,
                                text: data.text,
                                confirmButtonColor: "#66BB6A",
                                type: "error"
                            });  
                        }    
                    }
                });
            }
        });
    });

    // Confirm cancel change
    $('body').on('click' , '.btn-cancel', function() {
        var title = 'Bạn chắc không?'; //$(this).attr('data-title');
        var text = 'Bạn thật sự muốn hủy thay đổi?'//$(this).attr('data-text');
        var href = $(this).attr('value');
        swal({
            title: title,
            text: text,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Đồng ý!",
            cancelButtonText: "Không!",
            closeOnConfirm: false,
            closeOnCancel: true
        },
        function(isConfirm){
            if (isConfirm) {
                window.location.href = href;
            }
        });
    });
});