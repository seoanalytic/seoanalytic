$(document).ready(function() {

	$('#btn_admin_update_profile').on('click', function(e) {
        e.preventDefault();
        var url = $('#form_admin_update_profile').attr('action');
        var data = $('#form_admin_update_profile').serialize();
        $.ajax({
            type: 'post',
            url: url,
            data: data,
            success: function(res) {
                if(res.errors == null || res.errors == '') {
                    clear_error_profile('form_admin_update_profile');
                    $('#form_admin_update_profile .alert').remove();
                    $('#form_admin_update_profile').prepend('<div class="alert alert-success fade in alert-dismissable">'+
                        'Bạn đã cập nhật thông tin cá nhân thành công!</div>');
                }
                else {
                    $('#form_admin_update_profile .alert').remove();
                    clear_error_profile('form_admin_update_profile');
                    associate_errors(res.errors, $('#form_admin_update_profile'));
                }
            },
            error: function(res) {
                console.log(res);
            }
        });
    });



});

function clear_error_profile(id_form) {
        $('#' + id_form + ' .group-input').each(function() {
            $(this).removeClass('has-error');
            $(this).find('.help-block').text('');
        });
    }

function associate_errors(errors, form)
{
    $.each(errors, function(k, v) {
        var group = $('#' + k);
        group.addClass('has-error');
        group.find('.help-block').text(v);
    });
}