(function($) {
    /*=======================================
    =   LOGIN + REGISTER + UPDATEPROFILE   =
    =======================================*/
    // register
    $('#btn_register').on('click', function(e) {
        e.preventDefault();
        var url = $('#form_register').attr('action');
        var data = $('#form_register').serialize();
        $.ajax({
            type: 'post',
            url: url,
            data: data,
            success: function(res) {
                if(res.errors == null || res.errors == '') {
                    success_do(res);
                }
                else {
                    clear_error('form_register');
                    associate_errors(res.errors, $('#form_register'));
                }
            },
            error: function(res) {
                console.log(res);
            }
        });
    });
    // login
    $('#btn_login').on('click', function(e) {
        e.preventDefault();
        var url = $('#form_login').attr('action');
        var data = $('#form_login').serialize();
        $.ajax({
            type: 'post',
            url: url,
            data: data,
            success: function(res) {
                if(res.errors == null || res.errors == '') {
                    window.location.reload();
                }
                else {
                    clear_error('form_login');
                    associate_errors(res.errors, $('#form_login'));
                }
            },
            error: function(res) {
                console.log(res);
            }
        });
    });

    //updateProfile
    $('#btn_update_profile').on('click', function(e) {
        e.preventDefault();
        var url = $('#form_update_profile').attr('action');
        var data = $('#form_update_profile').serialize();
        $.ajax({
            type: 'post',
            url: url,
            data: data,
            success: function(res) {
                if(res.errors == null || res.errors == '') {
                    clear_error_profile('form_update_profile');
                    $('.navbar-right .do_login a.dropdown-toggle').html('CHÀO, '+res.data.name);
                    $('#form_update_profile .alert').remove();
                    $('#form_update_profile').prepend('<div class="alert alert-success fade in alert-dismissable">'+
                        'Bạn đã cập nhật thông tin cá nhân thành công!</div>');
                }
                else {
                    $('#form_update_profile .alert').remove();
                    clear_error_profile('form_update_profile');
                    associate_errors(res.errors, $('#form_update_profile'));
                }
            },
            error: function(res) {
                console.log(res);
            }
        });
    });
    /*=======================================
    =               CONTACT                =
    =======================================*/
    $('#btn_send_mail').on('click', function(e) {
        e.preventDefault();
        var url = $('#form_send_mail').attr('action');
        var data = $('#form_send_mail').serialize();
        $.ajax({
            type: 'post',
            url: url,
            data: data,
            success: function(res) {
                clear_error('form_send_mail');
                if (res.errors == null || res.errors == '') {
                    sweetAlert("Đã gửi thành công!", "Cảm ơn đã liên hệ với chúng tôi.", "success");
                } else {
                    associate_errors(res.errors, 'form_send_mail');
                }
            },
            error: function(res) {
                console.log(res);
            }
        });
    });
    /*=======================================
    =        VIEW / LESS HEADING           =
    =======================================*/
    $('a.btn-view-less').on('click', function() {
        if ($('.button-view-less a.btn-view-less').text() == 'Xem thêm') {
            $('.button-view-less a.btn-view-less').text('Rút gọn');
        } else {
            $('.button-view-less a.btn-view-less').text('Xem thêm');
        }
        $('.view-h').toggleClass('height-inherit');
    });

    /*=======================================
    =        VIEW / LESS INTERNAL LINK     =
    =======================================*/
    $('a.btn-view-less-link').on('click', function() {
        if ($('.button-view-less-link a.btn-view-less-link').text() == 'Xem thêm') {
            $('.button-view-less-link a.btn-view-less-link').text('Rút gọn');
        } else {
            $('.button-view-less-link a.btn-view-less-link').text('Xem thêm');
        }
        $('.view-link').toggleClass('height-link');
    });
     /*=======================================
    =        VIEW / LESS EXTERNAL LINK     =
    =======================================*/
    $('a.btn-view-less-exter').on('click', function() {
        if ($('.button-view-less-exter a.btn-view-less-exter').text() == 'Xem thêm') {
            $('.button-view-less-exter a.btn-view-less-exter').text('Rút gọn');
        } else {
            $('.button-view-less-exter a.btn-view-less-exter').text('Xem thêm');
        }
        $('.view-exter').toggleClass('height-exter');
    });

    // SUPPORT FUNCTION
    // 
            //
            //
            function clear_error(id_form) {
                $('#' + id_form + ' input').each(function(k,v) {
                    var group = $('#' + v['name']);
                    if(group.length) {
                        group.removeClass('has-error');
                        group.find('.help-block').text('');
                    }
                });
            }

            function clear_error_profile(id_form) {
                $('#' + id_form + ' .input-group').each(function() {
                console.log($(this));
                        $(this).removeClass('has-error');
                        $(this).find('.help-block').text('');
                });
            }

            function associate_errors(errors, form)
            {
                $.each(errors, function(k, v) {
                    var group = $('#' + k);
                    group.addClass('has-error');
                    group.find('.help-block').text(v);
                });
            }
})(jQuery);