/**/
$( window ).scroll( function() {
    if ( $( this ).scrollTop() > 0 ) {
        $( '.back-to-top' ).addClass( 'show-back-to-top' );
    } else {
        $( '.back-to-top' ).removeClass( 'show-back-to-top' );
    }
});

// Click event to scroll to top.
$( '.back-to-top' ).click( function() {
    $( 'html, body' ).animate( { scrollTop : 0 }, 800 );
    return false;
});

/**/

// $('a[href*=\\#]:not([href=\\#])').not('#myCarousel a, .modal-trigger a, .panel a').click(function(o){
//     if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') || location.hostname === this.hostname) {
//         var target = $(this.hash);
//         target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
//         if (target.length) {
//             if ($(".navbar").css("position") === "fixed") {
//                 $('html,body').animate({
//                     scrollTop: target.offset().top - 72
//                 }, 1000, 'swing');
//             } else {
//                 $('html,body').animate({
//                     scrollTop: target.offset().top
//                 }, 1000, 'swing');
//             }
//             return false;
//         }
//     }
// });


// Check distance to top and display back-to-top.
var hoverSpan = $('.btn_hover span');

$( ".btn_hover" ).mouseenter(function() {
    //add class enter on hover

    $(hoverSpan).removeClass('reset').addClass('enter');

})
    .mouseleave(function() {
        //remove class enter
        //add class leave and reset on complete

        $(hoverSpan).removeClass('enter').addClass('leave');

        setTimeout(function() {
            $(hoverSpan).removeClass('leave').addClass('reset');
        }, 10);

    });


/*----- -----------*/

$("#history_form .css_border:first-child").css(" border-top","1px solid black");
/*-----------------*/

$(function () {
    $('[data-toggle="popover"]').popover({
        placement:'auto',
        trigger:"hover"
    });
});
/*---------------------------------------------------*/
$(document).ready(function () {
    $('#chartValue').submit(function (event) {
        var chartValue=$(this).serializeArray();
        event.preventDefault();
        /*khi lây gia trị 1 form thì giá trị mặc định sẽ vào action cho nên phải có event để ngăn chặn lại,
        khi submit sẽ load lại trang*/
        /*        var x=Number($('#chartValue input[name=x]').val());
                var y=Number($('#chartValue input[name=y]').val());
                var z=Number($('#chartValue input[name=z]').val());*/
        var x=Number(chartValue[0].value);
        var y=Number(chartValue[1].value);
        var z=Number(chartValue[2].value);
        inputchart(x,y,z);//goi ham duoi lam nhiem vu
    });
    /*---------------------------------video-----------------------------------------*/
    $('.popup-youtube').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false,
        iframe: {
            patterns: {
                youtube: {
                    index: 'youtube.com/',
                    id: function (url) {
                        var m = url.match(/[\\?\\&]v=([^\\?\\&]+)/);
                        if (!m || !m[1]) return null;
                        return m[1];
                    },
                    src: '//www.youtube.com/embed/%id%?autoplay=1'
                }
            }
        }
    });
    /*--------------------------------navtop analytic-----------------------------------------*/
    var scroll_position=$(window).scrollTop();
    var nav_height=$(".topnav").height();
    if(scroll_position>nav_height){
        $(".topnav").css("position","fixed");
        $(".topnav").addClass("animated fadeInDown");
    }
    $(window).scroll(function () {

        var scroll_position=$(window).scrollTop();
        if(scroll_position< $(document).height()){
            $(".topnav a").css("border-bottom","none");
            $("a[href=\\#backlink]").css("border-bottom","2px solid #222222");
        }
        if(scroll_position< Math.floor($("#backlink").offset().top-$(".topnav").height())){

            $(".topnav a").css("border-bottom","none");
            $("a[href=\\#mobile]").css("border-bottom","2px solid #222222");
        }
        // if(scroll_position< $(document).height()){
        //     $(".topnav a").css("border-bottom","none");
        //     $("a[href=\\#mobile]").css("border-bottom","2px solid #222222");
        // }
        if(scroll_position< Math.floor($("#mobile").offset().top-$(".topnav").height())){

            $(".topnav a").css("border-bottom","none");
            $("a[href=\\#speed]").css("border-bottom","2px solid #222222");
        }
        if(scroll_position< Math.floor($("#speed").offset().top-$(".topnav").height())){

            $(".topnav a").css("border-bottom","none");
            $("a[href=\\#standard_SEO]").css("border-bottom","2px solid #222222");
        }
        // if(scroll_position< Math.floor($("#standard_SEO").offset().top-$(".topnav").height())){

        //     $(".topnav a").css("border-bottom","none");
        //     $("a[href=\\#Overview]").css("border-bottom","2px solid black");
        // }
        //scroll possition > height (header) #Overview").offset().top)-
        if(scroll_position>($("#header").height())){
            $("#header").css("display","none");
            $(".topnav").css("display","block");
        }
        else{
            $("#header").css("display","block");
            $(".topnav").css("display","none");
        }

    });
    $(".topnav a").click(function () {
        var href=$(this).attr("href");
        var nav_height=$(".topnav").height();
        var position=$(href).offset().top;

        /* $(".topnav a").css("border-bottom","none");
         $(this).css("border-bottom","6px solid white");*/
        $("html, body").animate({ scrollTop: position-nav_height }, 1000);
        return false;
    });

});

function demoFromHTML() {
    var pdf = new jsPDF('p', 'pt', 'letter');
    // source can be HTML-formatted string, or a reference
    // to an actual DOM element from which the text will be scraped.
    source = $('#HTML-TO-PDF')[0];

    // we support special element handlers. Register them with jQuery-style
    // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
    // There is no support for any other type of selectors
    // (class, of compound) at this time.
    specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        '#bypassme': function (element, renderer) {
            // true = "handled elsewhere, bypass text extraction"
            return true
        }
    };
    margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: 522
    };
    // all coords and widths are in jsPDF instance's declared units
    // 'inches' in this case
    pdf.fromHTML(
        source, // HTML string or DOM elem ref.
        margins.left, // x coord
        margins.top, { // y coord
            'width': margins.width, // max width of content on PDF
            'elementHandlers': specialElementHandlers
        },

        function (dispose) {
            // dispose: object with X, Y of the last line add to the PDF
            //          this allow the insertion of new lines after html
            pdf.save('Test.pdf');
        }, margins);
}
