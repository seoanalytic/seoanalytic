var pdfLoaderHTML = "<div class=\"windows8\">\n" +
    "\t<div class=\"wBall\" id=\"wBall_1\">\n" +
    "\t\t<div class=\"wInnerBall\"></div>\n" +
    "\t</div>\n" +
    "\t<div class=\"wBall\" id=\"wBall_2\">\n" +
    "\t\t<div class=\"wInnerBall\"></div>\n" +
    "\t</div>\n" +
    "\t<div class=\"wBall\" id=\"wBall_3\">\n" +
    "\t\t<div class=\"wInnerBall\"></div>\n" +
    "\t</div>\n" +
    "\t<div class=\"wBall\" id=\"wBall_4\">\n" +
    "\t\t<div class=\"wInnerBall\"></div>\n" +
    "\t</div>\n" +
    "\t<div class=\"wBall\" id=\"wBall_5\">\n" +
    "\t\t<div class=\"wInnerBall\"></div>\n" +
    "\t</div>\n" +
    "</div>";
var pdfOriginalHTML = "<span class=\"glyphicon glyphicon-download-alt\"> PDF</span>";

var ajaxStatus = {
    'mobileFriendly' : false,
    'mobileSnapshoot' : false,
    'websiteLoadTime' : false
};

$(document).ready(function(){
    $('#html2pdf').addClass('disabled');
	if(window.location.pathname == '/du-lieu')
	{
		// countdownTimmer = setInterval(callAJAXSeoAnalyst(seoAnalystFactor[factorIndexProcessing],300));
		callAJAXLoop();
	}
	else
	{
		console.log('out get-data');
	}
});

var seoAnalystFactor = [
	'mobileFriendly',
	'mobileSnapshoot',
	'websiteLoadTime'
];

var factorIndexProcessing = 0;
// var countdownTimmer;
function getParameterByName(name, url = false) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function changeAjaxStatus(criterion, status)
{
    ajaxStatus[criterion] = status;
    for(var criterion in ajaxStatus)
    {
        if(!ajaxStatus[criterion])
        {
            return false;
        }
    }
    if(getParameterByName('compare_id') == null)
    {
        setTimeout(function(){
            storeAnalyzeHistory()
        },2000);
    }

}

function storeAnalyzeHistory(){
    $.ajax({
        url: '/seo-analyst/storeAnalyzeHistory',
        type: 'POST',
        data: {
            user_id: $('#user_id').text(),
            url: $('.url-name a').attr('href'),
            html_content: JSON.stringify(analyzeHtmlContent())
        },
        success: function(result){
            console.log('storeAnalyzeHistory',result);
            // console.log(JSON.parse(result));
        },
        error: function(){
            console.log('storeAnalyzeHistory',result);
        }
    });

}

function seoAnalyst(factorName){
	if(seoAnalystFactor.indexOf('mobileFriendly')>-1){
		console.log('call function '+factorName);
		window[factorName](factorName);
	}
	else
	{
		console.log('factorName is not found.');
	}
	
}

function mobileSnapshoot(result){
	if(result)
	{
        $('#mobileSnapshoot').find('#circularG').remove();
        changeAjaxStatus('mobileSnapshoot',true);

	    $('#html2pdf').removeClass('disabled');
    	$('#mobileSnapshoot').prepend('<img class="get-content" id="picture-mobile" src="/mobile-snapshoot/'+result+'"/>');
        $('#mobileSnapshoot').prepend('<img  class="iphone" src="/mobile-snapshoot/iphone5s.png" />');
        $('#mobileSnapshoot').siblings('.report-icon').html('<i class="fa fa-info-circle" aria-hidden="true" data-toggle="popover" data-content="How your website look on mobile device."></i>');
	}

}

function mobileFriendly(result){
	if(result || result != null)
	{

		var OK = '<i class="fa fa-check-circle-o" aria-hidden="true" data-toggle="popover" data-content="OK"></i>';
		var NOTOK = '<i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>';
		result=JSON.parse(result);

        changeAjaxStatus('mobileFriendly',true);

        $('#fontSize').find('#circularG').remove();
		if(result.fontSize >= 14)
		{
            $('#fontSize').prepend('Kích cỡ font chữ của trang web bạn dễ đọc.');
            $('#fontSize').siblings('.report-icon').html(OK);
		}
		else {
            $('#fontSize').prepend('Kích cỡ font chữ của trang web bạn khó đọc. Font-size dễ đọc lớn hơn 13px');
            $('#fontSize').siblings('.report-icon').html(NOTOK);
		}

        $('#metaViewportTag').find('#circularG').remove();
		if(result.metaViewportTag)
		{
			$('#metaViewportTag').prepend('Chúc mừng, website của bạn có cấu hình viewport.');
            $('#metaViewportTag').siblings('.report-icon').html(OK);
		}
		else
		{
            $('#metaViewportTag').prepend('Rất tiếc, website của bạn chưa cấu hình viewport.'+
				'<button type="button" class="mar-t btn-sweet" data-toggle="modal" data-target="#metaViewportTagmyModal">Khắc phục</button>' +
                '<div class="modal fade" id="metaViewportTagmyModal" role="dialog">' +
                '<div class="modal-vertical-alignment-helper">' +
                '<div class="modal-dialog modal-vertical-align-center">' +
                '<div class="modal-content">' +
                '<div class="modal-header">\n' +
                '<button type="button" class="close" data-dismiss="modal">&times;</button>\n' +
                '<h4 class="modal-title"><b>Hướng dẫn - Cấu hình Viewport</b></h4>\n' +
                '</div>' +
                '<div class="modal-body">\n' +
                '<p>Bạn nên đặt thẻ metaviewport này vào cặp <code> &lt;head&gt;</code> trên trong mã HTML của website</p>\n' +
				'<pre>&lt;meta name="viewport" content="width=device-width, initial-scale=1"></pre>' +
                '</div>\n' +
                '<div class="modal-footer">\n' +
                '<button type="button" class="btn btn-default button-close" data-dismiss="modal">Đóng</button>\n' +
                '</div>\n' +
                '</div>\n' +
                '</div>\n' +
                '</div>\n' +
            '</div>');
            $('#metaViewportTag').siblings('.report-icon').html(NOTOK);
		}

        $('#scrollWidth').find('#circularG').remove();
		if(result.scrollWidth - result.viewportWidth <= 5)
		{
            $('#scrollWidth').prepend('Nội dung websites của bạn đều nằm trong vùng nhìn.');
            $('#scrollWidth').siblings('.report-icon').html(OK);

		}
		else
		{
            $('#scrollWidth').prepend('Nội dung websites của bạn đã vượt ra khỏi vùng nhìn.' +
                '<button type="button" class="mar-t btn-sweet" data-toggle="modal" data-target="#scrollWidthmyModal">Khắc phục</button>' +
                '<div class="modal fade" id="scrollWidthmyModal" role="dialog">' +
                '<div class="modal-vertical-alignment-helper">' +
                '<div class="modal-dialog modal-vertical-align-center">' +
                '<div class="modal-content">' +
                '<div class="modal-header">\n' +
                '<button type="button" class="close" data-dismiss="modal">&times;</button>\n' +
                '<h4 class="modal-title"><b>Khắc phục - Kích cỡ nội dung</b></h4>\n' +
                '</div>' +
                '<div class="modal-body">\n' +
                '<p>Để giải quyết vấn đề này, bạn nên:' +
                '<ul>\n' +
                '<li>Trước tiên,<a target="_blank" href="https://thachpham.com/web-development/html-css/lam-giao-dien-respoonsive.html">css cho chiều rộng của thiết bị</a>.</li>\n' +
				'<li>Sau đó, bạn cần có công cụ kiểm tra responsive của website.' +
				'<br> Ví dụ như công cụ <a target="_blank" href="http://lab.maltewassermann.com/viewport-resizer/">resize</a> hoặc developer tool của google chorm.</p>\n</li>' +
                '</ul>' +
                '</div>\n' +
                '<div class="modal-footer">\n' +
                '<button type="button" class="btn btn-default button-close" data-dismiss="modal">Đóng</button>\n' +
                '</div>\n' +
                '</div>\n' +
                '</div>\n' +
                '</div>\n' +
                '</div>');
            $('#scrollWidth').siblings('.report-icon').html(NOTOK);
		}
	}
}

function websiteLoadTime(loadtime){
    if(loadtime || loadtime != null)
    {
        changeAjaxStatus('websiteLoadTime',true);

        var OK = '<i class="fa fa-check-circle-o" aria-hidden="true" data-toggle="popover" data-content="OK"></i>';
        var NOTOK = '<i class="fa fa-times-circle" aria-hidden="true" data-toggle="popover" data-content="NOT OK"></i>';
        
        $('#websiteLoadTime').find('#circularG').remove();

        if(loadtime > 5000)
		{
            $('#websiteLoadTime').prepend('Thời gian tải trang web của bạn khoảng <b>'+loadtime/1000+' giây</b>, lớn hơn thời gian tải trang trung bình, chuẩn là <b>5 giây</b>.'+'<button type="button" class="mar-t btn-sweet" data-toggle="modal" data-target="#websiteLoadTimemyModal">Khắc phục</button>' +
				'<div class="modal fade" id="websiteLoadTimemyModal" role="dialog">' +
					'<div class="modal-vertical-alignment-helper">' +
						'<div class="modal-dialog modal-vertical-align-center">' +
							'<div class="modal-content">' +
								'<div class="modal-header">\n' +
               						 '<button type="button" class="close" data-dismiss="modal">&times;</button>\n' +
               						 '<h4 class="modal-title"><b>Hướng dẫn - Tốc độ load site</b></h4>\n' +
              					  '</div>' +
								'<div class="modal-body">\n' +
               						 '<p>Để giải quyết vấn đề này, bạn nên:</p>\n' +
                					'<ul>\n' +
                						'<li>Giảm số lượng request trên trang web của bạn</li>\n' +
                						'<li>Sử dụng Gzip để nén dữ liệu</li>\n' +
                						'<li>Sử dụng HTTP Caching để chuyển một bản copy các tài nguyên tĩnh phía Server xuống lưu ở dưới Client</li>\n' +
										'<li>Di chuyển tất cả các file CSS thành 1 file, nhúng file CSS ngoại tuyến vào HTML và minify file CSS </li>\n' +
                						'<li>Minify tất cả các file JS và nhúng mã ngoại tiếng vào file HTML</li>\n' +
                						'<li>Nhúng file CSS ngoại tuyến trước file ngoại tuyến JS</li>\n' +
										'<li>Đặt mã JS nằm ở cuối trang web của bạn</li>\n' +
               							'<li>Tối ưu hóa ảnh</li>\n' +
										'<li>Giảm số lượng plugin</li>\n' +
                						'<li>Reduce redirects</li>\n' +
               						 '</ul>' +
								'</div>\n' +
								'<div class="modal-footer">\n' +
									'<button type="button" class="btn btn-default button-close" data-dismiss="modal">Đóng</button>\n' +
               					 '</div>\n' +
                			'</div>\n' +
                		'</div>\n' +
               		 '</div>\n' +
                '</div>');
            $('#websiteLoadTime').siblings('.report-icon').html(NOTOK);
		}
		else
		{
            $('#websiteLoadTime').prepend('Chúc mừng thời gian tải trang web của bạn khoảng <b>'+loadtime/1000+' giây</b>, dưới thời gian tải trung bình, chuẩn là <b> 5 giây</b>');
            $('#websiteLoadTime').siblings('.report-icon').html(OK);
		}
    }
}


function callAJAXSeoAnalyst (factorName){
	$.ajax({
		url: '/seo-analyst/'+factorName,
		type: 'POST',
		data: {
			url: $('.url-name a').attr('href')
			// url: 'http://tinnhan.viettel.vn'
		},
		success: function(result){
			window[factorName](result);
		},
		error: function(){
			console.log(factorName +' timeout.');
		}
	});

}


function callAJAXLoop(){
	setTimeout(function(){
		if(factorIndexProcessing < seoAnalystFactor.length){
			callAJAXLoop();
			callAJAXSeoAnalyst(seoAnalystFactor[factorIndexProcessing]);
			factorIndexProcessing++;
		}
	},500);
}

function analyzeHtmlContent()
{
    var html_content = {
        factor:{
            standard: [],
            speed: [],
            mobile: [],
            backlink: []
        },
        analytic_detail:{
            standard: [],
            speed: [],
            mobile: [],
            backlink:[]
        },
        mobile_snapshoot:{
            name: 'Hình ảnh',
            iphone: '/mobile-snapshoot/iphone5s.png',
            snapshoot: ''
        },
        url: $("#results-header a").attr('href')
    };

    $('.factor.standard_seo').each(function(index){
        // console.log($(this).siblings('.report-icon').find('i').attr('data-content'));

        html_content.factor.standard.push($(this).html());

        // html_content.factor.standard.push(
        //     {
        //         html: $(this).html(),
        //         isPassed: false
        //     }
        // );
    });
    $('.factor.speed_seo').each(function(index){
        html_content.factor.speed.push($(this).html());
    });
    $('.factor.mobile_seo').each(function(index){
        if(index < $('.analytic-detail.mobile_seo').length-1) {
            html_content.factor.mobile.push($(this).html());
        }
    });
    $('.factor.backlink_seo').each(function(index){
        html_content.factor.backlink.push($(this).html());
    });

    $('.analytic-detail.standard_seo').each(function(index){
        var isPassed = true;
        if($(this).siblings('.report-icon').find('i').attr('data-content')=="NOT OK"){
            isPassed= false;
        }
        var modalBody=$(this).find('.modal .modal-body').html() === undefined ? "" : $(this).find('.modal .modal-body').html();
        var modalTitle=$(this).find('.modal .modal-title').html() === undefined ? "" : $(this).find('.modal .modal-title').html();
        var html = $(this).clone();
        html.find('button').remove();
        html.find('.modal').remove();
        html = html.html();
        html_content.analytic_detail.standard.push({
            html: html,
            isPassed: isPassed,
            modalTitle: modalTitle,
            modalBody: modalBody
        });
    });
    $('.analytic-detail.speed_seo').each(function(index){
        var isPassed = true;
        if($(this).siblings('.report-icon').find('i').attr('data-content')=="NOT OK"){
            isPassed= false;
        }
        var modalBody=$(this).find('.modal .modal-body').html() === undefined ? "" : $(this).find('.modal .modal-body').html();
        var modalTitle=$(this).find('.modal .modal-title').html() === undefined ? "" : $(this).find('.modal .modal-title').html();
        var html = $(this).clone();
        html.find('button').remove();
        html.find('.modal').remove();
        html = html.html();
        html_content.analytic_detail.speed.push({
            html: html,
            isPassed: isPassed,
            modalTitle: modalTitle,
            modalBody: modalBody
        });
    });
    $('.analytic-detail.mobile_seo').each(function(index){
        var isPassed = true;
        if($(this).siblings('.report-icon').find('i').attr('data-content')=="NOT OK"){
            isPassed= false;
        }
        var modalBody=$(this).find('.modal .modal-body').html() === undefined ? "" : $(this).find('.modal .modal-body').html();
        var modalTitle=$(this).find('.modal .modal-title').html() === undefined ? "" : $(this).find('.modal .modal-title').html();
        var html = $(this).clone();
        html.find('button').remove();
        html.find('.modal').remove();
        html = html.html();
        if(index < $('.analytic-detail.mobile_seo').length-1) {
            html_content.analytic_detail.mobile.push({
                html: html,
                isPassed: isPassed,
                modalTitle: modalTitle,
                modalBody: modalBody

            });
        }
    });
    html_content.mobile_snapshoot.snapshoot = $('img#picture-mobile').attr('src');

    $('.analytic-detail.backlink_seo').each(function(index){
        var isPassed = true;
        if($(this).siblings('.report-icon').find('i').attr('data-content')=="NOT OK"){
            isPassed= false;
        }
        var modalBody=$(this).find('.modal .modal-body').html() === undefined ? "" : $(this).find('.modal .modal-body').html();
        var modalTitle=$(this).find('.modal .modal-title').html() === undefined ? "" : $(this).find('.modal .modal-title').html();
        var html = $(this).clone();
        html.find('button').remove();
        html.find('.modal').remove();
        html = html.html();
        html_content.analytic_detail.backlink.push({
            html: html,
            isPassed: isPassed,
            modalTitle: modalTitle,
            modalBody: modalBody
        });
    });
    return html_content;
}


$(document).ready(function () {
    $('#html2pdf').click(function(){
        $(this).html(pdfLoaderHTML);
        //Object, JSON
        //object: key, value


        $.ajax({
            url: '/seo-analyst/html2pdf',
            type: 'POST',
            data: {
                html_content: analyzeHtmlContent()
            },
            success: function(pdfFileName){
                //window.open(window.location.protocol+'//'+window.location.hostname+'/'+'html2pdf'+'/'+pdfFileName);
                $('#html2pdf').html(pdfOriginalHTML);
                $('#html2pdf_download').remove();
                //$('body').append('<a id="html2pdf_download" style="display: none" download="SEOAnalytics-report" href="'+window.location.protocol+'//'+window.location.hostname+'/'+'html2pdf'+'/'+pdfFileName+'" download>click</a>');
                $('body').append('<a id="html2pdf_download" style="display: none" download="SEOAnalytics-report" href="'+window.location.protocol+'//'+window.location.hostname+':28/'+'html2pdf'+'/'+pdfFileName+'" download>click</a>');
                document.getElementById('html2pdf_download').click();

                $('#html2pdf_download').remove();
            },
            error: function(){
                $('#html2pdf').html(pdfOriginalHTML);
                console.log('html to pdf timeout.');
            }
        });

    });
});