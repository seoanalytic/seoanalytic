jQuery(function($) {
    "use strict";

    var SLZ = window.SLZ || {};

    /*=======================================
    =             MAIN FUNCTION             =
    =======================================*/
    SLZ.headerFunction = function() {
    };

    SLZ.mainFunction = function() {
        
    };
    $(document).ready(function() {
        SLZ.headerFunction();
        SLZ.mainFunction();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });

        $('.form_ajax_submit').on('submit', function(event) {
            event.preventDefault();
            var target = $(this);
            var targetAppend = $(this).attr('data-target');
            var url = $(this).attr('action');
            var method = $(this).attr('method');
            var data = $(this).serialize();
            $.ajax({
                url: url,
                data: data,
                type: method,
                success: function(response) {
                    clear_content(target);
                    // ['type': 'success|error' msg': <>, 'url': <>]
                    handleSuccessAjaxRequest(response, targetAppend);
                },
                error: function (response) {
                    if(response.status == 422) {
                        association_errors(response['responseJSON'], target);
                    } else if(response.status == 401) {
                        //$('#login').modal('show');
                        console.log(response);
                    } else {
                        console.log(response);
                    }
                }
            })
        });

    });
    
    /** FUNCTION SUPPORT */
    function association_errors(errors, target) {
        $(target).find('.has-error').removeClass('has-error');
        $(target).find('.help-block').html('');
        $.each(errors, function(k, v) {
            var input = $(target).find('[name='+ k + ']');
            var field = $(input).parent();
            field.addClass('has-error');
            field.find('.help-block').html('<strong>' + v + '</strong>');
        });
    }

    function clear_content(target) {
        $(target).find('textarea').each(function (index, item) {
            $(item).val('');
        });
    }

    function handleSuccessAjaxRequest(response, targetAppend = null) {
        if(response.msg != null && response.msg != undefined) {
            showNotify(response.type, null, response.msg);
        }
        if(response.url != null && response.url != undefined) {
            window.location.href = response.url; // will be changed
        }
        if(response.html != null && response.html != undefined && $(targetAppend).length > 0) {
            $(targetAppend).append(response.html);
        }
        if(response.reload) {
            location.reload();
        }
    }

    function handleErrorAjaxRequest() {

    }
});